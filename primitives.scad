// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#Values_and_Data_Types
// A value in OpenSCAD is one of the following:
// * Number     (like 42)
// * Boolean    (like true)
// * String     (like "foo")
// * Range      (like [0: 1: 10])
// * Vector     (like [1, 2, 3])
// * Undefined  (just undef)

// This file is about type checking and manipulating the above types, which I'll call primitives
// Many languages have "dot access" to member functions and data of objects, OpenSCAD doesn't.
// If I conceptually want a method 'object.method(arg)', I have to implement it as a global function:
//      method(object, arg)
// For quick reference these are the conceptual methods provided in this file:
//      * object.isUndef(): Boolean
//      * object.isBoolean(): Boolean
//      * object.isNan(): Boolean
//      * object.isNumber(): Boolean
//      * object.isString(): Boolean
//      * object.isList(): Boolean

//      * value.orElse(default): ValueOrDefault

//      * String.sliceStr(start, end): String

//      * List.emptyList(): List
//      * List.isEmpty(): Boolean
//      * List.size(): Number
//      * List.append(value): List
//      * List.insert(index, value): List
//      * List.set(index, value): List
//      * List.remove(index): List
//      * List.replaceAll(replace, with): List
//      * List.contains(value): Boolean
//      * List.indexOf(value): Number
//      * List.findAll(value): IndexList
//      * List.head(): Value
//      * List.last(): Value
//      * List.slice(start, end): List
//      * List.take(n): List
//      * List.takeRight(n): List
//      * List.drop(n): List
//      * List.dropRight(): List
//      * List.reverse(): List
//      * List.appendAll(otherList): List
//      * List.zipWithIndex(): TupleList
//      * List.zip(otherList): TupleList
//      * TupleList.unzip(): [List, List]
//      * List.flatten(): FlattenedList
//      * range(range): List
//      * List.sum(): Number
//      * List.merge(otherSortedList): List
//      * List.sort(): SortedList

//      * emptyMap(): Map
//      * Map.keys(): KeyList
//      * Map.values(): ValueList
//      * Map.containsKey(key): Boolean
//      * Map.containsValue(value): Boolean
//      * Map.getEntry(key): KeyValuePair
//      * Map.get(key): Value
//      * Map.getOrElse(key, default): ValueOrDefault
//      * Map.put(key, value): Map
//      * Map.putAll(otherMap): Map
//      * Map.deepGet(dottedString): Value
//      * Map.deepPut(dottedString): Map
//      * Map.deepPutAll(otherMap): Map

//      * origin: Vector
//      * X: Vector
//      * Y: Vector
//      * Z: Vector

//      * Vector.x(): Number
//      * Vector.y(): Number
//      * Vector.z(): Number
//      * Vector.r(): Number
//      * Vector.theta(): Number
//      * Vector.phi(): Number
//      * polarToRect(r, theta, phi): Vector
//      * Vector.unit(): UnitVector
//      * Vector.length(): Number
//      * Vector.dot(otherVector): Number
//      * Vector.crs(otherVector): OrthogonalVector
//      * Vector.angle(otherVector): Number
//      * Vector.scalarPrj(otherVector): Number
//      * Vector.prj(otherVector): Vector
//      * Vector.prj_perp(otherVector): Vector
//      * Vector.rotate_toward(otherVector): Vector
//      * Vector.rotate_away(otherVector): Vector

//      * translate_m(vector): TransformationMatrix
//      * scale_m(vector, center): TransformationMatrix
//      * rotate_m(vector, axis_dir, center): TransformationMatrix
//      * mirror_m(norm_dir, center): TransformationMatrix
//      * Vector.transform_pt(transformationMatrix): Vector
//      * VectorList.transform_pts(transformationMatrix): VectorList
//      * VectorList.transform(transformationMatrix): VectorList

use <error.scad>
/*****************************
 * type tests for PRIMITIVES *
 *****************************/
 // Input:  any reference (a variable)
 // Output: a boolean for whether the reference is of the corresponding primitive type
function isUndef(object) = object == undef;
function isBoolean(object) = (object == true) || (object == false);
function isNan(object) = object != object;
// should isNumber() exclude Nan?  An Nan would mess up geometry...
function isNumber(object) = !isUndef(sign(object)) && !isNan(object);
function isString(object) = object == str(object);
function isList(object) = !isUndef(len(object)) && !isString(object);

/**********************************
 * functionality for undef values *
 **********************************/
// Input:   a value
//          a default
// Output:  if the value was undef, then the default, otherwise the value itself
function orElse(value, default) =
    value != undef ? value
    : default;

/***************************************
 * functionality for PRIMITIVE Strings *
 ***************************************/

// Input:   a string,
//          a start index,
//          an end index
// Output:  the slice of the string from start index (inclusive) until end index (exclusive)
function sliceStr(string, start, end, acc="") =
    start >= end || start >= len(string) ? acc
    : sliceStr(string, start+1, end, str(acc, string[start]));

/*************************************************************
 * functionality for PRIMITIVE (non-type-safe) LISTS         *
 * these functions don't make assumptions about the contents *
 *************************************************************/

// get(list, i) == list[i] // reserving get() for Maps, sorry Lists.

// Input:   none
// Output:  a new empty list
function emptyList() =
    [];

// Input:   a list
// Output:  true if the list is empty, false if the list has some elements
function isEmpty(list) =
    list==[];

// Input:   a list
// Output:  the number of values in the list
function size(list) =
    len(list);

// Input:   a list,
//          a new element
// Output:  a copy of the input list with the value appended
function append(list, value) =
    concat(list, [value]);

// Input:   a list,
//          an index within the list,
//          a value to insert into the list at that index
// Output:  a copy of the input list, longer by one, with the insertion made
function insert(list, index, value) =
    concat( take(list, index),
            [value],
            drop(list, index) );

// Input:   a list,
//          an index within that list,
//          the value that should replace the old value at that index
// Output:  a copy of the input list with the replacement made
function set(list, index, value) =
    list == undef ? undef
    : [ for (i=[0:len(list)-1])
          i != index ? list[i]
          : value
      ];

// Input:   a list,
//          an index within that list
// Output:  a copy of the input list, shorter by one, with the removal made
function remove(list, index) =
    list == undef ? undef
    : [ for (i=[0:len(list)-1])
          if (i != index) list[i]
      ];

// Input:   a list,
//          a value to replace
//          a value to replace it with
// Output:  a copy of the list with all occurrences of 'replace' replaced with 'with'
function replaceAll(list, replace, with) =
    list == undef ? undef
    : [ for (v=list) v == replace ? with
                    : v
     ];

// Input:   a list,
//          a value to test for presence
// Output:  true if the value is present, false otherwise
function contains(list, value) =
    indexOf(list, value) != undef;

// Input:   a list,
//          a value whose index to find
//          the index at which to start looking (defaulted to 0)
// Output:  the index of the value's first position greater than or equal to from, or undef if not present
function indexOf(list, value, from=0) =
    from + search([value], drop(list, from), num_returns_per_match=0)[0][0];

// Input:   a list,
//          a value to find all indices of
// Output:  a list of all the indices where the value occurs (or an empty list if it's not present)
function findAll(list, value) =
    search([value], list, num_returns_per_match=0)[0];

// Input:   a list
// Output:  the first value in the list
function head(list) =
    list[0];
// tail(list) == drop(list, 1)

// Input:   a list
// Output:  the last value in the list
function last(list) =
    list[len(list)-1];

// Input:   a list,
//          a start index,
//          an end index
// Output:  a copy of the slice of the list from start (inclusive) to end (exclusive) indices
function slice(list, start, end) =
    list == undef ? undef
    : start >= end ? []
    : start <= 0 && len(list) < end ? list
    : [ for(i=[0:len(list)-1])
            if (start <= i && i < end) list[i]
      ];

// Input:   a list,
//          a number of values n
// Output:  the first n values in the list
function take(list, n) =
    slice(list, 0, n);

// Input:   a list,
//          a number of values n
// Output:  the last (right) n values in the list
function takeRight(list, n) =
    drop(list, len(list)-n);

// Input:   a list,
//          a number of values n
// Output:  a copy of the list without the first n values
function drop(list, n) =
    slice(list, n, len(list));

// Input:   a list,
//          a number of values n
// Output:  a copy of the list without the last (right) n values
function dropRight(list, n) =
    take(list, len(list)-n);

// Input:   a list
// Output:  a copy of the list reversed
function reverse(list) =
    list == undef ? undef
    : [ for (i=[len(list)-1:-1:0])
          list[i]
      ];

// Input:   two lists
// Output:  a new list where the input lists are concatenated end to end
function appendAll(l1, l2) =
    concat(l1, l2);

// Input:   a list
// Output:  a list of pairs (a pair is a list of length 2) of the list element and it's index
function zipWithIndex(list) =
    list == undef ? undef
    : [ for(i=[0:len(list)-1])
          [list[i], i]
      ];

// Input:   two lists
// Output:  a list of pairs (a pair is a list of length 2) of the list elements,
//          differences in lengths are filled in with undef's
function zip(l1, l2) =
    let (   l1Len = len(l1),
            l2Len = len(l2),
            zipLen = max(l1Len, l2Len)
    )
    zipLen == 0 ? []
    : [ for( i=[0:zipLen-1] )
            [l1[i], l2[i]]
      ];


/********************************************************
 * functionality for PRIMITIVE (non-type-safe) LISTS    *
 * these functions make assumptions about the contents  *
 ********************************************************/

// Input:   a list of pairs
// Output:  a single pair (a list with two elements)
//          the first element is the list of all the first elements from the input list of pairs
//          the second element is the list of all the second elements from the input list of pairs
function unzip(ll) =
    ll == undef ? undef
    : [ [ for(l=ll)
              l[0]
        ],
        [ for(l=ll)
              l[1]
        ]
      ];

// Input:   a nested list
// Ouptut:  a list of all the sub-lists of the input list concatenated together
function flatten(list) =
    list == undef ? undef
    : [ for (i=list, v=i)
          v
      ];

// Input:   a range (like [1:10])
// Output:  a list containing all the elements of the range
function range(rng) =
    rng == undef ? undef
    : [ for (i=rng)
          i
      ];

// Input:   a list of numbers
// Output:  the sum of the numbers
function sum(list, i=0, acc=0) =
    i >= len(list) ? acc
    : sum(list, i+1, acc + list[i]);

// from https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#Sorting_a_vector
//function quicksort(l) =
//    len(l)<=1 ? l
//    : let(    pivot   = arr[floor(len(l)/2)],
//              lesser  = [ for (e=l) if (e  < pivot) e ],
//              equal   = [ for (e=l) if (e == pivot) e ],
//              greater = [ for (e=l) if (e  > pivot) e ]
//      )
//      concat(quicksort(lesser), equal, quicksort(greater));

// Input:   two sorted lists of elements which can be compared with each other
// Output:  a single list which contains all of the elements from both lists, sorted
function merge(l1, l2, acc=[]) =
    l1 == undef || l2 == undef ? undef
    : isEmpty(l1) ? concat(acc, l2)
    : isEmpty(l2) ? concat(acc, l1)
    : l2[0] < l1[0] ? merge(l2, l1, acc)
    : merge(drop(l1, 1), l2, append(acc, l1[0]));

// Input:   a list of elements which can be compared with each other
// Output:  a copy of the list in which the elements are sorted
function sort(list) =
    len(list)<=1 ? list
    : let(  m = floor(len(list)/2),
            first = slice(list, 0, m),
            last = slice(list, m, len(list))
      )
      merge(sort(first), sort(last));


/*************************************************************
 * functionality for PRIMITIVE (non-type-safe) MAPS          *
 * these functions assume the lists contain key/value pairs  *
 *************************************************************/

function emptyMap() = emptyList();
function keys(map) = unzip(map)[0];
function values(map) = unzip(map)[1];
function containsKey(map, key) = contains(keys(map), key);
function containsValue(map, value) = contains(values(map), value);
function _keyIndex(map, key) = search([key], map, num_returns_per_match=0)[0][0];
// A map is a list of pairs, but search() works not just with pairs, but with tuples.
// getEntry() is looking for the first of a pair as the key
// since getEntry() is based on search, it can be used (as a hack) to get an entire tuple
// getEntry([[1, 2, 3], ["a", "b", "c"]], "a") == ["a", "b", "c"]
// get([[1, 2, 3], ["a", "b", "c"]], "a") == "b"  // as if from the pair ["a", "b"]
function getEntry(map, key) = map[_keyIndex(map, key)];
function get(map, key) = getEntry(map, key)[1];
function getOrElse(map, key, default) =
    containsKey(map, key) ? get(map, key)
    : default;
function put(map, key, value) = 
    let (   entry = [key, value],
            index = _keyIndex(map, key)
    )
    index == undef ? append(map, entry)
    : set(map, index, entry);
function _putAll(map, i, acc) =
    i < 0 ? acc
    : _putAll(map, i-1, put(acc, map[i][0], map[i][1]));
function putAll(map, otherMap) = _putAll(otherMap, len(otherMap)-1, map);

/************************************************************
 * functionality for PRIMITIVE (non-type-safe) MAPS         *
 * these functions assume the lists contain key/value pairs *
 * and that the keys are strings with no '.'s               *
 ************************************************************/

function deepGet(map, dottedKey) =
    let (   dotIndex = indexOf(dottedKey, ".")
    )
    dotIndex == undef ? get(map, dottedKey)
    : let ( keyHead = sliceStr(dottedKey, 0, dotIndex),
            keyTail = sliceStr(dottedKey, dotIndex+1, len(dottedKey))
      )
      deepGet(get(map, keyHead), keyTail);

function deepPut(map, dottedKey, value) =
    let (   dotIndex = indexOf(dottedKey, ".")
    )
    dotIndex == undef ? put(map, dottedKey, value)
    :   let (   keyHead = sliceStr(dottedKey, 0, dotIndex),
                keyTail = sliceStr(dottedKey, dotIndex+1, len(dottedKey))
        )
        put(map, keyHead,
            containsKey(map, keyHead) ? deepPut(get(map, keyHead), keyTail, value)
            :   deepPut(emptyMap(), keyTail, value)
        );

function _deepPutAll(map, i, acc) =
    i < 0 ? acc
    : _deepPutAll(map, i-1, deepPut(acc, map[i][0], map[i][1]));
function deepPutAll(map, otherMap) =
    _deepPutAll(otherMap, len(otherMap)-1, map);

/**********************************************************
 * functionality for PRIMITIVE (non-type-safe) 3D VECTORS *
 * these functions assume the lists contain 3 numbers     *
 **********************************************************/

origin = [0, 0, 0];
// using caps, does this avoid conflict with a variable named 'x'?
X = [1, 0, 0];
Y = [0, 1, 0];
Z = [0, 0, 1];

function x(v) = v[0];
function y(v) = v[1];
function z(v) = v[2];

function r(v) = length(v);
function theta(v) = atan(y(v)/x(v));
function phi(v) = acos(z(v)/length(v));

function polarToRect(r, theta, phi) =
    [   r*cos(theta)*sin(phi),
        r*sin(theta)*sin(phi),
        r*cos(phi)  ];

function unit(v) = v / length(v);

function length(v) = norm(v);
function dot(v1, v2) = v1 * v2;         // TLA alias for built-in
// crs (and cross) returns vector orthogonal to both v1 and v2
// not symmetrical: crs(v1, v2) == -crs(v2, v1)
function crs(v1, v2) = cross(v1, v2);   // TLA alias for built-in

// angle between vectors
// cos(t) == dot(v1, v2) / (length(v1) * length(v2))
function angle(v1, v2) = acos(dot(v1, v2) / (length(v1) * length(v2)));

// projection of v1 onto v2
function scalar_prj(v1, v2) = dot(v1, v2) / length(v2);
function prj(v1, v2) = scalar_prj(v1, v2) * unit(v2);
function prj_perp(v1, v2) = v1 - prj(v1, v2);

// rotation of v1 toward, or away from v2
function rotate_toward(v1, v2, a, center = origin) = 
    let (   v1p = v1 - center,
            v2p = v2 - center,
            mutual_perp = cross(v1p, v2p),
            rot_m = rotate_m(a, mutual_perp),
            resultp = transform_pt(v1p, rot_m)
    )
    resultp + center;
function rotate_away(v1, v2, a, center = origin) =
    rotate_toward(v1, v2, -a, center);


/************************************************************
 * functionality for transformation matrices for 3D VECTORS *
 * from https://en.wikipedia.org/wiki/Transformation_matrix *
 ************************************************************/

function translate_m(v) =
    let (   x = ensure_3d(v)
    )
    [   [1, 0, 0, x[0]],
        [0, 1, 0, x[1]],
        [0, 0, 1, x[2]],
        [0, 0, 0,    1] ];

function scale_m(v, center=origin) = // assumes v is 3d
	translate_m(center) *  // this happens last
	[   [v[0],    0,    0, 0],
	    [   0, v[1],    0, 0],
	    [   0,    0, v[2], 0],
	    [   0,    0,    0, 1],
	] *
	translate_m(-center); // this happens first

function rotate_m(angle, axis_dir, center=origin) = // assumes axis_dir and center are 3d
	let (	unit_axis = unit(axis_dir),
			l = unit_axis[0],
			m = unit_axis[1],
			n = unit_axis[2],
			ct = cos(angle),  // ct for cos(theta)
			st = sin(angle),  // st for sin(theta)
			cc = (1 - ct)	  // cc for complement of cos(theta)
    )
	translate_m(center) * // this happens last
	[	[   l*l*cc + ct,  m*l*cc - n*st,  n*l*ct + m*st,  0 ],
		[ l*m*cc + n*st,    m*m*cc + ct,  n*m*cc - l*st,  0 ],
		[ l*n*cc - m*st,  m*n*cc + l*st,    n*n*cc + ct,  0 ],
		[             0,              0,              0,  1 ]
	] *
	translate_m(-center); // this happens first

function mirror_m(norm_dir, center=origin) = // assumes norm_dir and center are 3d
	let (	unit_norm = unit(norm_dir),
			a = unit_norm[0],
			b = unit_norm[1],
			c = unit_norm[2]
    )
	translate_m(center) * // this happens last
	[	[1 - 2*a*a,    -2*a*b,    -2*a*c, 0],
		[   -2*a*b, 1 - 2*b*b,    -2*b*c, 0],
		[   -2*a*c,    -2*b*c, 1 - 2*c*c, 0],
		[        0,         0,         0, 1]
	] *
	translate_m(-center); // this happens first

// These could probably be improved...  Would like interfaces to remain 3d,
// but would like to use matrix multiplication (instead of for loop) to transform a whole list of points

function def0(r) =
    r != undef ? r
    : 0;

function def0_pt(pt) = [for (c=pt) def0(c)];

function def0_pts(pts) = [for (pt=pts) def0_pt(pt)];

function to_2d_pt(pt) = [pt[0], pt[1]];

function to_3d_pt(pt) = [pt[0], pt[1], pt[2]];

function to_2d_pts(pts) = [for (pt=pts) to_2d_pt(pt)];

function to_3d_pts(pts) = [for (pt=pts) to_3d_pt(pt)];

function ensure_3d(pt) =
    len(pt) == 2 ? [pt[0], pt[1], 0]
    : [pt[0], pt[1], pt[2]];

// automatically convert 2d points to 3d, and return the dimensions of the input point
function transform_pt(pt, transform_m) =
    let (   pt_3d = ensure_3d(pt),
            pt_4d = transform_m * concat(pt_3d, 1),
            result = len(pt) == 2 ? to_2d_pt(pt_4d)
                     : to_3d_pt(pt_4d)
    )
    result;

function transform_pts(pts, transform_m) =
    [for (pt=pts) transform_pt(pt, transform_m)];

function transform(pts, transform_m) =
    transform_pts(pts, transform_m);






/*********
 * TESTS *
 *********/

module testPrimitiveMap() {
    m1 = emptyMap();
//    require(m1 == [], "m1 was wrong");
    require(size(m1) == 0, "m1.size was wrong");
    require(keys(m1) == [], "m1.keys was wrong");
    require(values(m1) == [], "m1.values was wrong");
    require(containsKey(m1, "x") == false, "m1.containsKey('x') was wrong");
    require(containsValue(m1, 3) == false, "m1.containsValue(3) was wrong");
    require(get(m1, "x") == undef, "m1.get('x') was wrong");

    m2 = put(m1, "y", 2);
//    require(m2 == [["y", 2]], "m2 was wrong");
    require(size(m2) == 1, "m2.size was wrong");
    require(keys(m2) == ["y"], "m2.keys was wrong");
    require(values(m2) == [2], "m2.values was wrong");
    require(containsKey(m2, "x") == false, "m2.containsKey('x') was wrong");
    require(containsValue(m2, 3) == false, "m2.containsValue(3) was wrong");
    require(get(m2, "x") == undef, "m2.get('x') was wrong");

    m3 = put(m2, "x", 3);
//    require(m3 == [["y", 2], ["x", 3]], "m3 was wrong");
    require(size(m3) == 2, "m3.size was wrong");
    require(keys(m3) == ["y", "x"], "m3.keys was wrong");
    require(values(m3) == [2, 3], "m3.values was wrong");
    require(containsKey(m3, "x") == true, "m3.containsKey('x') was wrong");
    require(containsValue(m3, 3) == true, "m3.containsValue(3) was wrong");
    require(get(m3, "x") == 3, "m3.get('x') was wrong");

    m4 = put(m3, "z", 1);
//    require(m4 == [["y", 2], ["x", 3], ["z", 1]], "m4 was wrong");
    require(size(m4) == 3, "m4.size was wrong");
    require(keys(m4) == ["y", "x", "z"], "m4.keys was wrong");
    require(values(m4) == [2, 3, 1], "m4.values was wrong");
    require(containsKey(m4, "x") == true, "m4.containsKey('x') was wrong");
    require(containsValue(m4, 3) == true, "m4.containsValue(3) was wrong");
    require(get(m4, "x") == 3, "m4.get('x') was wrong");

    m5 = put(m4, "x", 17);
//    require(m5 == [["y", 2], ["x", 17], ["z", 1]], "m5 was wrong");
    require(size(m5) == 3, "m5.size was wrong");
    require(keys(m5) == ["y", "x", "z"], "m5.keys was wrong");
    require(values(m5) == [2, 17, 1], "m5.values was wrong");
    require(containsKey(m5, "x") == true, "m5.containsKey('x') was wrong");
    require(containsValue(m5, 3) == false, "m5.containsValue(3) was wrong");
    require(get(m5, "x") == 17, "m5.get('x') was wrong");

    m6 = deepPutAll(m5, [["y.deep", 19], ["w.deep", 4]]);
//    require(m6 == [["y", [2, ["deep", 19]]], ["x", 17], ["z", 1], ["w", [["deep", 4]]]], "m6 was wrong");
    require(size(m6) == 4, "m6.size was wrong");
    require(keys(m6) == ["y", "x", "z", "w"], "m6.keys was wrong");
    require(values(m6) == [[2, ["deep", 19]], 17, 1, [["deep", 4]]], "m6.values was wrong");
    require(containsKey(m6, "x") == true, "m6.containsKey('x') was wrong");
    require(containsValue(m6, 3) == false, "m6.containsValue(3) was wrong");
    require(get(m6, "x") == 17, "m6.get('x') was wrong");
}

module exploreIndexOf() {
    echo(str("indexOf([], 0): ", indexOf([], 0)));
    echo(str("indexOf([0], 0): ", indexOf([0], 0)));
    echo(str("indexOf([1, 0], 0): ", indexOf([1, 0], 0)));
    echo(str("indexOf([1, 0, 0], 0): ", indexOf([1, 0, 0], 0)));
    echo(str("indexOf([1, 0, 0], 0, from=2): ", indexOf([1, 0, 0], 0, from=2)));
            
    echo(str("indexOf([], 'a'): ", indexOf([], "a")));
    echo(str("indexOf(['a'], 'a'): ", indexOf(["a"], "a")));
    echo(str("indexOf(['b', 'a'], 'a'): ", indexOf(["b", "a"], "a")));
    echo(str("indexOf(['b', 'a', 'a'], 'a'): ", indexOf(["b", "a", "a"], "a")));
    echo(str("indexOf(['b', 'a', 'a'], 'a', from=2): ", indexOf(["b", "a", "a"], "a", from=2)));
}

module exploreFindAll() {
    echo(str("findAll([], 0): ", findAll([], 0)));
    echo(str("findAll([0], 0): ", findAll([0], 0)));
    echo(str("findAll([1, 0], 0): ", findAll([1, 0], 0)));
    echo(str("findAll([1, 0, 0], 0): ", findAll([1, 0, 0], 0)));

    echo(str("findAll([], 'a'): ", findAll([], "a")));
    echo(str("findAll(['a'], 'a'): ", findAll(["a"], "a")));
    echo(str("findAll([1, 'a'], 'a'): ", findAll(["b", "a"], "a")));
    echo(str("findAll([1, 'a', 'a'], 'a'): ", findAll(["b", "a", "a"], "a")));
}

testPrimitiveMap();
//exploreIndexOf();
//exploreFindAll();
