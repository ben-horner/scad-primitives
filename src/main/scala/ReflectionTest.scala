import java.lang.reflect.{AnnotatedType, Constructor, Method, Type}
import java.security.ProtectionDomain

import scala.reflect.{ClassTag, _}

case class Test[Z](x: Int, y: String, z: Z)

object ReflectionTest {

  def print(name: String, value: Boolean): Unit = {
    println(name)
    println(s"\t$value")
  }

  def print(name: String, value: Int): Unit = {
    println(name)
    println(s"\t$value")
  }

  def print(name: String, value: String): Unit = {
    println(name)
    println(s"\t$value")
  }

  def print[T](name: String, values: Array[T]): Unit = {
    println(name)
    if (Option(values).nonEmpty) {
      if (values.isEmpty){
        println("\tempty")
      } else {
        values.foreach { value =>
          println(s"\t$value")
        }
      }
    } else {
      println("\tnull")
    }
  }

  def print(name: String, value: Package): Unit = {
    println(name)
    println(s"\t$value")
  }

//  def print(name: String, value: Class[_]): Unit = {
//    println(name)
//    println(s"\t$value")
//  }

  def print(name: String, value: Type): Unit = {
    println(name)
    println(s"\t$value")
  }

  def print(name: String, value: AnnotatedType): Unit = {
    println(name)
    println(s"\t$value")
  }

  def print(name: String, value: Constructor[_]): Unit = {
    println(name)
    println(s"\t$value")
  }

  def print(name: String, value: Method): Unit = {
    println(name)
    println(s"\t$value")
  }

  def print(name: String, value: ProtectionDomain): Unit = {
    println(name)
    println(s"\t$value")
  }



  def printClass[A: ClassTag]: Unit = {
    val clazz: Class[_] = classTag[A].runtimeClass
    print("getName", clazz.getName)
    print("getSimpleName", clazz.getSimpleName)
    print("getCanonicalName", clazz.getCanonicalName)
    print("getPackage", clazz.getPackage)

    print("getAnnotations", clazz.getAnnotations)
    print("getDeclaredAnnotations", clazz.getDeclaredAnnotations)

    print("getClasses", clazz.getClasses)
    print("getDeclaredClasses", clazz.getDeclaredClasses)

    print("getInterfaces", clazz.getInterfaces)

    print("getFields", clazz.getFields)
    print("getDeclaredFields", clazz.getDeclaredFields)

    print("getConstructors", clazz.getConstructors)
    print("getDeclaredConstructors", clazz.getDeclaredConstructors)

    print("getMethods", clazz.getMethods)
    print("getDeclaredMethods", clazz.getDeclaredMethods)



    print("desiredAssertionStatus", clazz.desiredAssertionStatus)
    print("getAnnotatedInterfaces", clazz.getAnnotatedInterfaces)
    print("getAnnotatedSuperclass", clazz.getAnnotatedSuperclass)
    print("getClass", clazz.getClass)
    print("getComponentType", clazz.getComponentType)
    print("getDeclaringClass", clazz.getDeclaringClass)

    print("getEnclosingClass", clazz.getEnclosingClass)
    print("getEnclosingConstructor", clazz.getEnclosingConstructor)
    print("getEnclosingMethod", clazz.getEnclosingMethod)
    print("getEnumConstants", clazz.getEnumConstants)

    print("getGenericInterfaces", clazz.getGenericInterfaces)
    print("getGenericSuperclass", clazz.getGenericSuperclass)
    print("getModifiers", clazz.getModifiers)
    print("getProtectionDomain", clazz.getProtectionDomain)
    print("getSigners", clazz.getSigners)
    print("getSuperclass", clazz.getSuperclass)
    print("getTypeName", clazz.getTypeName)
    print("getTypeParameters", clazz.getTypeParameters)

    print("isAnnotation", clazz.isAnnotation)
    print("isAnonymousClass", clazz.isAnonymousClass)
    print("isArray", clazz.isArray)
    print("isEnum", clazz.isEnum)
    print("isInterface", clazz.isInterface)
    print("isLocalClass", clazz.isLocalClass)
    print("isMemberClass", clazz.isMemberClass)
    print("isPrimitive", clazz.isPrimitive)
    print("isSynthetic", clazz.isSynthetic)
    print("toGenericString", clazz.toGenericString)
    print("toString", clazz.toString)

  }

  def main(args: Array[String]): Unit = {
    printClass[Test[Boolean]]
  }

}
