package bkh.scad.solid

case class PntVar(name: String, valueOpt: Option[PointExpr] = None) extends PointExpr {
  override def value: Pnt = valueOpt.get.value
  override def simplify: PointExpr = this
  override def toString: String = name
}
