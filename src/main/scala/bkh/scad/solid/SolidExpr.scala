package bkh.scad.solid

import bkh.matrix.Matrix
import bkh.scad.numeric.Num._
import bkh.scad.numeric.{NumVar, NumericExpr}
import bkh.scad.ToScad

trait SolidExpr extends ToScad {
  import SolidExpr._

  def translated(vector: VectorExpr): SolidExpr = Translate(vector)(this).simplify
  def rotated(roll: NumericExpr, pitch: NumericExpr, yaw: NumericExpr): SolidExpr =
    Rotate(roll, pitch, yaw)(this).simplify
  def scaled(x: NumericExpr, y: NumericExpr, z: NumericExpr): SolidExpr =
    Scale(x, y, z)(this).simplify
  def mirrored(vector: VectorExpr): SolidExpr = Mirror(vector)(this).simplify
  def resized(x: NumericExpr, y: NumericExpr, z: NumericExpr): SolidExpr = Resize(x, y, z)(this).simplify
  def multmatrixed(m: Matrix): SolidExpr = MultMatrix(m)(this).simplify
  def transformed(m: Matrix): SolidExpr = MultMatrix(m)(this).simplify
  def colored(name: String, alpha: NumericExpr=1): SolidExpr =
    Color.Builder().name(name).alpha(alpha).build(this).simplify
  def colored(r: NumericExpr, g: NumericExpr, b: NumericExpr): SolidExpr =
    Color.Builder().r(r).b(b).g(g).build(this).simplify
  def colored(r: NumericExpr, g: NumericExpr, b: NumericExpr, alpha: NumericExpr): SolidExpr =
    Color.Builder().r(r).b(b).g(g).alpha(alpha).build(this).simplify

  def minkowskiedWith(se: SolidExpr): SolidExpr = Minkowski(this, se)
  def hulledWith     (ses: Seq[SolidExpr]) = Hull        (this +: ses).simplify
  def unionedWith    (ses: Seq[SolidExpr]) = Union       (this +: ses).simplify
  def differencedWith(ses: Seq[SolidExpr]) = Difference  (this, ses).simplify
  def intersectedWith(ses: Seq[SolidExpr]) = Intersection(this +: ses).simplify

  // mulmatrix // 3d only?

  def disabled    : SolidExpr = Disable(this)
  def shownAlone  : SolidExpr = ShowOnly(this)
  def highlighted : SolidExpr = Highlight(this)
  def backgrounded: SolidExpr = Background(this)

  // scala only supports !, ~, + and - as unary operators, so I tried to keep things intuitive.
  // you can of course use the functions too.

  def unary_- : SolidExpr = disabled     // in OpenSCAD this is *
  def unary_! : SolidExpr = shownAlone   // in OpenSCAD this is still !
  def unary_+ : SolidExpr = highlighted  // in OpenSCAD this is #
  def unary_~ : SolidExpr = backgrounded // in OpenSCAD this is %

  def simplify: SolidExpr = this
}

case class SphereExpr(radius: NumericExpr) extends SolidExpr {
  override def toString: String = s"sphere($radius);"
}
object SphereExpr {
  case class Builder(radius: Option[NumericExpr]=None, diameter: Option[NumericExpr]=None) {
    def radius(radius: NumericExpr): Builder = this.copy(radius = Some(radius), diameter = None)
    def diameter(diameter: NumericExpr): Builder = this.copy(radius = None, diameter = Some(diameter))
    def build(): SphereExpr = SphereExpr(radius.getOrElse(diameter.get / 2))
  }
}

case class BoxExpr(x: NumericExpr, y: NumericExpr, z: NumericExpr) extends SolidExpr {
  override def toString: String = s"cube([$x, $y, $z]);"
}

case class CylinderExpr(height: NumericExpr, r1: NumericExpr, r2: NumericExpr) extends SolidExpr {
  override def toString: String = s"cylinder($height, $r1, $r2);"
}

case class PolyhedronExpr(points: Seq[PointExpr], faces: Seq[Seq[Int]], convexity: NumericExpr) extends SolidExpr {
  override def toScad: Seq[String] =
    Seq("polyhedron(points=[") ++
      ToScad.indent(ToScad.mkStringSeq(points.flatMap(_.toScad), ",")) ++
      Seq("],faces=[") ++
      ToScad.indent(ToScad.mkStringSeq(faces.map(face => face.mkString("[", ",", "]")), ",")) ++
      Seq("],", s"convexity=$convexity);")
}

case class VarSolid(name: String) extends SolidExpr {
  override def simplify: VarSolid = this
  override def toString: String = name
}

object SolidExpr {
  case class Translate(vector: VectorExpr)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"translate($vector){") ++
        ToScad.indent(se.toScad) ++
        Seq("}")
    override def simplify: SolidExpr = this
  }
  case class Rotate(roll: NumericExpr, pitch: NumericExpr, yaw: NumericExpr)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"rotate([$roll, $pitch, $yaw]){") ++
        ToScad.indent(se.toScad) ++
        Seq("}")
    override def simplify: SolidExpr = this
  }
  case class Scale(x: NumericExpr, y: NumericExpr, z: NumericExpr)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"scale([$x, $y, $z]){") ++
        ToScad.indent(se.toScad) ++
        Seq("}")
    override def simplify: SolidExpr = this
  }
  case class Mirror(vector: VectorExpr)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"mirror($vector){") ++
        ToScad.indent(se.toScad) ++
        Seq("}")
    override def simplify: SolidExpr = this
  }
  case class Resize(x: NumericExpr, y: NumericExpr, z: NumericExpr)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"resize([$x, $y, $z]){") ++
        ToScad.indent(se.toScad) ++
        Seq("}")
    override def simplify: SolidExpr = this
  }
  case class MultMatrix(m: Matrix)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"multmatrix(${m.rows.head.mkString("[ [", ", ", "],")}") ++
        m.rows.drop(1).dropRight(1).map(_.mkString("             [", ", ", "],")) ++
        Seq(m.rows.last.mkString("             [", ", ", "] ]){")) ++
        ToScad.indent(se.toScad) ++
        Seq("}")
    override def simplify: SolidExpr = this
  }
  case class UnionFor(loopVar: NumVar, start: NumericExpr, step: NumericExpr, end: NumericExpr)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"for ($loopVar=[$start:$step:$end]){") ++
        ToScad.indent(se.toScad) ++
        Seq("}")
  }
  case class IntersectFor(loopVar: NumVar, start: NumericExpr, step: NumericExpr, end: NumericExpr)(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"intersection_for ($loopVar=[$start:$step:$end]){") ++
        ToScad.indent(se.toScad) ++
        Seq("}")
  }
  case class LinearExtrude(height: NumericExpr)(se2d: bkh.scad.flat.FlatExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"linear_extrude($height){") ++
        ToScad.indent(se2d.toScad) ++
        Seq("}")
  }
  case class RotateExtrude(angle: NumericExpr)(se2d: bkh.scad.flat.FlatExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq(s"rotate_extrude(angle=$angle){") ++
        ToScad.indent(se2d.toScad) ++
        Seq("}")
  }
  case class Color(name: Option[String],
                   r: Option[NumericExpr],
                   g: Option[NumericExpr],
                   b: Option[NumericExpr],
                   alpha: Option[NumericExpr])(se: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] = {
      val sb = StringBuilder.newBuilder.append("color(")
      if (name.nonEmpty) sb.append(s""""${name.get}"""")
      if (r.nonEmpty) sb.append(s"[${r.get}")
      if (b.nonEmpty) sb.append(s",${b.get}")
      if (g.nonEmpty) sb.append(s",${g.get}]")
      if (alpha.nonEmpty) sb.append(s",${alpha.get}")
      sb.append("){")
      Seq(sb.toString()) ++
        ToScad.indent(se.toScad) ++
        Seq("}")
    }
  }
  object Color {
    case class Builder(_name: Option[String] = None,
                       _r: Option[NumericExpr] = None,
                       _g: Option[NumericExpr] = None,
                       _b: Option[NumericExpr] = None,
                       _alpha: Option[NumericExpr] = None) {
      def name(name: String): Builder = this.copy(_name = Some(name))
      def r(r: NumericExpr): Builder = this.copy(_r = Some(r))
      def g(g: NumericExpr): Builder = this.copy(_g = Some(g))
      def b(b: NumericExpr): Builder = this.copy(_b = Some(b))
      def alpha(alpha: NumericExpr) = this.copy(_alpha = Some(alpha))
      // TODO: thow exception when building with illegal combination of arguments
      def build: SolidExpr => SolidExpr = Color(_name, _r, _g, _b, _alpha)
    }
  }
  case class Minkowski(se1: SolidExpr, se2: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq("minkowski(){") ++
        ToScad.indent(se1.toScad) ++
        ToScad.indent(se2.toScad) ++
        Seq("}")
  }
  case class Hull(ses: Seq[SolidExpr]) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq("hull(){") ++
        ToScad.indent(ses.flatMap(_.toScad)) ++
        Seq("}")
  }
  case class Union(ses: Seq[SolidExpr]) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq("union(){") ++
        ToScad.indent(ses.flatMap(_.toScad)) ++
        Seq("}")
  }
  case class Difference(startWith: SolidExpr, subtractAll: Seq[SolidExpr]) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq("difference(){") ++
        ToScad.indent(startWith.toScad) ++
        ToScad.indent(subtractAll.flatMap(_.toScad)) ++
        Seq("}")
  }
  case class Intersection(ses: Seq[SolidExpr]) extends SolidExpr {
    override def toScad: Seq[String] =
      Seq("intersection(){") ++
        ToScad.indent(ses.flatMap(_.toScad)) ++
        Seq("}")
  }

  class Modifier(modifier: Char, fe: SolidExpr) extends SolidExpr {
    override def toScad: Seq[String] = {
      val enabled = fe.toScad
      (modifier + enabled.head) +: enabled.tail
    }
  }
  case class Disable   (fe: SolidExpr) extends Modifier('*', fe)
  case class ShowOnly  (fe: SolidExpr) extends Modifier('!', fe)
  case class Highlight (fe: SolidExpr) extends Modifier('#', fe)
  case class Background(fe: SolidExpr) extends Modifier('%', fe)

}