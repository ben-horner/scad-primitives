package bkh.scad.solid

import bkh.scad.numeric.Num._
import bkh.scad.numeric.NumericExpr

case class Vec(override val x: NumericExpr, override val y: NumericExpr, override val z: NumericExpr) extends VectorExpr {

  override def value: Vec = this

  override def unary_- : Vec = Vec(-x, -y, -z)
  override def unary_+ : Vec = this
  override def * (scalar: NumericExpr): Vec = Vec(this.x * scalar, this.y * scalar, this.z * scalar)
  override def *:(scalar: NumericExpr): Vec = this * scalar
  override def / (scalar: NumericExpr): Vec = this * (1 / scalar)
  def +(that: Vec): Vec = Vec(this.x + that.x, this.y + that.y, this.z + that.z)
  def -(that: Vec): Vec = this + -that
  def dot(that: Vec): NumericExpr = this.x * that.x + this.y * that.y + this.z * that.z
  def *(that: Vec): NumericExpr = this.dot(that)
  def cross(that: Vec): Vec = Vec( // https://en.wikipedia.org/wiki/Cross_product
    this.y * that.z - this.z * that.y,
    this.x * that.z - this.z * that.x,
    this.x * that.y - this.y * that.x)
  def x(that: Vec): Vec = this.cross(that)

  override def simplify: VectorExpr = this
  override def toString: String = s"[$x,$y,$z]"
}

object Vec {
  val ZERO: Vec = Vec(0, 0, 0)
  val X: Vec = Vec(1, 0, 0)
  val Y: Vec = Vec(0, 1, 0)
  val Z: Vec = Vec(0, 0, 1)
}