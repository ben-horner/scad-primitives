package bkh.scad.solid

import bkh.scad.numeric.{Num, NumericExpr}
import bkh.scad.ToScad

// The whole purpose of the PointExpr is to limit the operations between points and vectors (type safety)
trait PointExpr extends ToScad {
  import PointExpr._

  def value: Pnt

  def +(ve: VectorExpr): PointExpr = PlusVector(this, ve).simplify
  def -(ve: VectorExpr): PointExpr = MinusVector(this, ve).simplify
  def -(pe: PointExpr): VectorExpr = Minus(this, pe).simplify

  def x: NumericExpr = GetX(this).simplify
  def y: NumericExpr = GetY(this).simplify
  def z: NumericExpr = GetZ(this).simplify

  def simplify: PointExpr
}

object PointExpr {

  case class GetX(pe: PointExpr) extends NumericExpr {
    override def value: Num = pe.value.v.value.x.value
    override def simplify: NumericExpr = pe match {
      case v: Vec => v.x
      case simplified => this
    }
    override def toString: String = s"($pe)[0]"
  }

  case class GetY(pe: PointExpr) extends NumericExpr {
    override def value: Num = pe.value.v.value.y.value
    override def simplify: NumericExpr = pe match {
      case v: Vec => v.y
      case simplified => this
    }
    override def toString: String = s"($pe)[1]"
  }

  case class GetZ(pe: PointExpr) extends NumericExpr {
    override def value: Num = pe.value.v.value.z.value
    override def simplify: NumericExpr = pe match {
      case v: Vec => v.z
      case simplified => this
    }
    override def toString: String = s"($pe)[2]"
  }

  case class PlusVector(pe: PointExpr, ve: VectorExpr) extends BinaryOpPointExpr(pe, ve, _ + _, "+")
  case class MinusVector(pe: PointExpr, ve: VectorExpr) extends BinaryOpPointExpr(pe, ve, _ - _, "-")
  case class Minus(pe1: PointExpr, pe2: PointExpr) extends FuncPPVectorExpr(pe1, pe2, _ - _, "-")

  // TODO: should f() be (Pnt, Vec) => Pnt (instead of VectorExpr)
  class BinaryOpPointExpr(pe: PointExpr, ve: VectorExpr, f: (Pnt, VectorExpr) => Pnt, symbol: String) extends PointExpr {
    override def value: Pnt = f(pe.value, ve)
    override def simplify: PointExpr = pe match {
      case p: Pnt => f(p, ve)
      case simplified => this
    }
    override def toString: String = s"($pe$symbol$ve)"
  }

  class FuncPPVectorExpr(pe1: PointExpr, pe2: PointExpr, f: (Pnt, Pnt) => VectorExpr, symbol: String) extends VectorExpr {
    override def value: Vec = f(pe1.value, pe2.value).value
    override def simplify: VectorExpr =(pe1, pe2) match {
      case (p1: Pnt, p2: Pnt) => f(p1, p2)
      case simplified => this
    }
    override def toString: String = s"$pe1$symbol$pe2"
  }

}
