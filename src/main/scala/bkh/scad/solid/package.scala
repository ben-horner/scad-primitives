package bkh.scad

import bkh.matrix.Matrix
import bkh.scad.numeric.Num._
import bkh.scad.numeric.{NumVar, NumericExpr}
import bkh.scad.solid.SolidExpr._
import bkh.scad.solid.VectorExpr.{Cross, Dot}

package object solid {
  def vec(x: NumericExpr, y: NumericExpr, z: NumericExpr): VectorExpr = Vec(x, y, z)
  def dot(ve1: VectorExpr, ve2: VectorExpr): NumericExpr = Dot(ve1, ve2).simplify
  def cross(ve1: VectorExpr, ve2: VectorExpr): VectorExpr = Cross(ve1, ve2).simplify

  def pnt(x: NumericExpr, y: NumericExpr, z: NumericExpr): PointExpr = Pnt(Vec(x, y, z))

  def sphere(radius: NumericExpr): SphereExpr = SphereExpr(radius)
  def cube(size: NumericExpr): BoxExpr = BoxExpr(size, size, size)
  def box(x: NumericExpr, y: NumericExpr, z: NumericExpr): BoxExpr = BoxExpr(x, y, z)
  def cylinder(height: NumericExpr, radius: NumericExpr): CylinderExpr = CylinderExpr(height, radius, radius)
  def cylinder(height: NumericExpr, r1: NumericExpr, r2: NumericExpr): CylinderExpr = CylinderExpr(height, r1, r2)
  def polyhedron(points: Seq[PointExpr], faces: Seq[Seq[Int]], convexity: NumericExpr): PolyhedronExpr = PolyhedronExpr(points, faces, convexity)

  def translate(vector: VectorExpr)(se: SolidExpr): SolidExpr = se.translated(vector)
  def rotate(roll: NumericExpr, pitch: NumericExpr, yaw: NumericExpr)(se: SolidExpr): SolidExpr = se.rotated(roll, pitch, yaw)
  def scale(x: NumericExpr, y: NumericExpr, z: NumericExpr)(se: SolidExpr): SolidExpr = se.scaled(x, y, z)
  def resize(x: NumericExpr, y: NumericExpr, z: NumericExpr)(se: SolidExpr): SolidExpr = se.resized(x, y, z)
  def mirror(vector: VectorExpr)(se: SolidExpr): SolidExpr = se.mirrored(vector)
  def multmatrix(m: Matrix)(se: SolidExpr): SolidExpr = se.multmatrixed(m)
  def transform(m: Matrix)(se: SolidExpr): SolidExpr = se.transformed(m)
  def unionFor(loopVar: NumVar, start: NumericExpr, step: NumericExpr, end: NumericExpr)(se: SolidExpr): UnionFor =
    UnionFor(loopVar, start, step, end)(se)
  def unionFor(loopVar: NumVar, start: NumericExpr, end: NumericExpr)(se: SolidExpr): UnionFor =
    UnionFor(loopVar, start, 1, end)(se)
  def intersectFor(loopVar: NumVar, start: NumericExpr, step: NumericExpr, end: NumericExpr)(se: SolidExpr): IntersectFor =
    IntersectFor(loopVar, start, step, end)(se)
  def intersectFor(loopVar: NumVar, start: NumericExpr, end: NumericExpr)(se: SolidExpr): IntersectFor =
    IntersectFor(loopVar, start, 1, end)(se)
  def minkowski(se1: SolidExpr, se2: SolidExpr): SolidExpr = se1.minkowskiedWith(se2)

  // unfortunately the functions below (which allow the use of lists of things) can't use curly braces {}
  def hull(ses: SolidExpr*): Hull = Hull(ses)
  def union(ses: SolidExpr*): Union = Union(ses)
  def difference(ses: SolidExpr*): Difference = Difference(startWith = ses.head, subtractAll = ses.tail)
  def intersection(ses: SolidExpr*): Intersection = Intersection(ses)

  def disable   (fe: SolidExpr): Disable = Disable(fe)
  def showOnly  (fe: SolidExpr): ShowOnly = ShowOnly(fe)
  def highlight (fe: SolidExpr): Highlight = Highlight(fe)
  def background(fe: SolidExpr): Background = Background(fe)

}
