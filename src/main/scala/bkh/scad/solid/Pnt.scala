package bkh.scad.solid

import bkh.scad.solid.Vec.ZERO

case class Pnt(v: VectorExpr) extends PointExpr {

  override def value: Pnt = this

  override def +(ve: VectorExpr): Pnt = Pnt(this.v + ve)
  override def -(ve: VectorExpr): Pnt = Pnt(this.v - ve)
  def -(that: Pnt): VectorExpr = that.v - this.v

  override def simplify: PointExpr = this
  override def toString: String = v.toString
}

object Pnt {
  val ORIGIN: Pnt = Pnt(ZERO)
}