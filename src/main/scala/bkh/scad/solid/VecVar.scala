package bkh.scad.solid

case class VecVar(name: String, valueOpt: Option[VectorExpr] = None) extends VectorExpr {
  override def value: Vec = valueOpt.get.value
  override def simplify: VectorExpr = this
  override def toString: String = name
}
