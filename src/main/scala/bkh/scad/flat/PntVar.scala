package bkh.scad.flat

case class PntVar(name: String, valueOpt: Option[PointExpr] = None) extends PointExpr {
  override def value: Pnt = {
    require(valueOpt.isDefined, s"cannot get value for PntVar with name $name is abstract, a name only")
    valueOpt.get.value
  }
  override def simplify: PointExpr = this
  override def toString: String = name
}
