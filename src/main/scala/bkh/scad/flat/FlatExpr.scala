package bkh.scad.flat

import bkh.scad.bool.Bool.FALSE
import bkh.scad.numeric.Num._
import bkh.scad.bool.BooleanExpr
import bkh.scad.numeric.{NumVar, NumericExpr}
import bkh.scad.ToScad


trait FlatExpr extends ToScad {
  import FlatExpr._

  def translated(vector: VectorExpr): FlatExpr = Translate(vector)(this).simplify   // TODO: replace Translate with Transform
  def rotated(angle: NumericExpr): FlatExpr = Rotate(angle)(this).simplify          // TODO: replace Rotate with Transform
  def scaled(x: NumericExpr, y: NumericExpr): FlatExpr = Scale(x, y)(this).simplify // TODO: replace Scale with Transform
  def mirrored(vector: VectorExpr): FlatExpr = Mirror(vector)(this).simplify        // TODO: replace Mirror with Transform
  def resized(x: NumericExpr, y: NumericExpr): FlatExpr = Resize(x, y)(this).simplify
  def colored(name: String, alpha: NumericExpr=1): FlatExpr =
    Color.Builder().name(name).alpha(alpha).build(this).simplify
  def colored(r: NumericExpr, g: NumericExpr, b: NumericExpr): FlatExpr =
    Color.Builder().r(r).b(b).g(g).build(this).simplify
  def colored(r: NumericExpr, g: NumericExpr, b: NumericExpr, alpha: NumericExpr): FlatExpr =
    Color.Builder().r(r).b(b).g(g).alpha(alpha).build(this).simplify
  def offsetted(radius: NumericExpr) =
    Offset.Builder().radius(radius).build(this).simplify
  def offsetted(delta: NumericExpr, chamfer: BooleanExpr = FALSE) =
    Offset.Builder().delta(delta).chamfer(chamfer).build(this).simplify
  def hulledWith     (fes: Seq[FlatExpr]) = Hull        (this +: fes).simplify
  def unionedWith    (fes: Seq[FlatExpr]) = Union       (this +: fes).simplify
  def differencedWith(fes: Seq[FlatExpr]) = Difference  (this, fes).simplify
  def intersectedWith(fes: Seq[FlatExpr]) = Intersection(this +: fes).simplify

  def linearExtruded(height: NumericExpr) = bkh.scad.solid.SolidExpr.LinearExtrude(height)(this).simplify
  def rotateExtruded(angle: NumericExpr) = bkh.scad.solid.SolidExpr.RotateExtrude(angle)(this).simplify

  def disable   : FlatExpr = Disable(this)
  def showOnly  : FlatExpr = ShowOnly(this)
  def highlight : FlatExpr = Highlight(this)
  def background: FlatExpr = Background(this)

  // scala only supports !, ~, + and - as unary operators, so I tried to keep things intuitive.
  // you can of course use the functions too.

  def unary_- : FlatExpr = disable    // in OpenSCAD this is *
  def unary_! : FlatExpr = showOnly
  def unary_+ : FlatExpr = highlight  // in OpenSCAD this is #
  def unary_~ : FlatExpr = background // in OpenSCAD this is %

  def simplify: FlatExpr = this
}


case class CircleExpr(radius: NumericExpr) extends FlatExpr {
  override def toString = s"circle($radius);"
  override def simplify: FlatExpr = this
}
object CircleExpr {
  case class Builder(radius: Option[NumericExpr]=None, diameter: Option[NumericExpr]=None) {
    def radius(radius: NumericExpr): Builder = this.copy(radius = Some(radius), diameter = None)
    def diameter(diameter: NumericExpr): Builder = this.copy(radius = None, diameter = Some(diameter))
    def build(): CircleExpr = CircleExpr(radius.getOrElse(diameter.get / 2))
  }
}

case class RectangleExpr(width: NumericExpr, height: NumericExpr) extends FlatExpr {
  override def toString = s"square([$width,$height]);"
}
object RectangleExpr {
  def square(size: NumericExpr): RectangleExpr = RectangleExpr(size, size)
}

// known limitation: not currently supporting variables for paths (or contents of paths)
case class PolygonExpr(points: Seq[PointExpr], paths: Option[Seq[Seq[Int]]] = None) extends FlatExpr {
  override def toScad: Seq[String] = {
    var result = Seq("polygon([") ++
    ToScad.indent(ToScad.mkStringSeq(points.flatMap(_.toScad), ","))
    if (paths.nonEmpty)
      result = result ++ Seq("],[") ++
        ToScad.indent(ToScad.mkStringSeq(paths.get.map(path => path.mkString("[", ",", "]")), ","))
    result = result ++ Seq("]);")
    result
  }
}



case class TextExpr(text: String,
                    _size: Option[NumericExpr] = None,
                    _font: Option[String] = None,
                    _hAlign: Option[String] = None,
                    _vAlign: Option[String] = None,
                    _spacing: Option[NumericExpr] = None,
                    _direction: Option[String] = None,
                    _language: Option[String] = None,
                    _script: Option[String] = None) extends FlatExpr {
  def size(size: NumericExpr) = this.copy(_size = Some(size))
  def font(font: String) = this.copy(_font = Some(font))
  def hAlign(hAlign: String) = this.copy(_hAlign = Some(hAlign))
  def vAlign(vAlign: String) = this.copy(_vAlign = Some(vAlign))
  def spacing(spacing: NumericExpr) = this.copy(_spacing = Some(spacing))
  def direction(direction: String) = this.copy(_direction = Some(direction))
  def language(language: String) = this.copy(_language = Some(language))
  def script(script: String) = this.copy(_script = Some(script))
  override def toScad = {
    val sb = StringBuilder.newBuilder.append(s"text($text")
    if (_size.nonEmpty) sb.append(s",size=${_size.get}")
    if (_font.nonEmpty) sb.append(s",font=${_font.get}")
    if (_hAlign.nonEmpty) sb.append(s",halign=${_hAlign.get}")
    if (_vAlign.nonEmpty) sb.append(s",valign=${_vAlign.get}")
    if (_spacing.nonEmpty) sb.append(s",spacing=${_spacing.get}")
    if (_direction.nonEmpty) sb.append(s",direction=${_direction.get}")
    if (_language.nonEmpty) sb.append(s",language=${_language.get}")
    if (_script.nonEmpty) sb.append(s",script=${_script.get}")
    sb.append(");")
    Seq(sb.toString())
  }
}

case class VarFlat(name: String) extends FlatExpr {
  override def simplify: FlatExpr = this
  override def toString: String = name
}

object FlatExpr {
  case class Translate(vector: VectorExpr)(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq(s"translate($vector){") ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
    override def simplify: FlatExpr = this // TOOD: replace with Transform, and combine transformations in simplify
  }
  case class Rotate(angle: NumericExpr)(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq(s"rotate($angle){") ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
    override def simplify: FlatExpr = this // TOOD: replace with Transform, and combine transformations in simplify
  }
  case class Scale(x: NumericExpr, y: NumericExpr)(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq(s"scale([$x, $y]){") ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
    override def simplify: FlatExpr = this // TOOD: replace with Transform, and combine transformations in simplify
  }
  case class Mirror(vector: VectorExpr)(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq(s"mirror($vector){") ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
    override def simplify: FlatExpr = this // TOOD: replace with Transform, and combine transformations in simplify
  }
  case class Resize(x: NumericExpr, y: NumericExpr)(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq(s"resize([$x, $y]){") ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
    override def simplify: FlatExpr = this
  }
  case class UnionFor(loopVar: NumVar, start: NumericExpr, step: NumericExpr, end: NumericExpr)(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq(s"for ($loopVar=[$start:$step:$end]){") ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
  }
  case class IntersectFor(loopVar: NumVar, start: NumericExpr, step: NumericExpr, end: NumericExpr)(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq(s"intersection_for ($loopVar=[$start:$step:$end]){") ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
  }
  case class Color(name: Option[String],
                   r: Option[NumericExpr],
                   g: Option[NumericExpr],
                   b: Option[NumericExpr],
                   alpha: Option[NumericExpr])(fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] = {
      val sb = StringBuilder.newBuilder.append("color(")
      if (name.nonEmpty) sb.append(s""""${name.get}"""")
      if (r.nonEmpty) sb.append(s"[${r.get}")
      if (b.nonEmpty) sb.append(s",${b.get}")
      if (g.nonEmpty) sb.append(s",${g.get}]")
      if (alpha.nonEmpty) sb.append(s",${alpha.get}")
      sb.append("){")
      Seq(sb.toString()) ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
    }
  }
  object Color {
    case class Builder(_name: Option[String] = None,
                       _r: Option[NumericExpr] = None,
                       _g: Option[NumericExpr] = None,
                       _b: Option[NumericExpr] = None,
                       _alpha: Option[NumericExpr] = None) {
      def name(name: String): Builder = this.copy(_name = Some(name))
      def r(r: NumericExpr): Builder = this.copy(_r = Some(r))
      def g(g: NumericExpr): Builder = this.copy(_g = Some(g))
      def b(b: NumericExpr): Builder = this.copy(_b = Some(b))
      def alpha(alpha: NumericExpr) = this.copy(_alpha = Some(alpha))
      // TODO: thow exception when building with illegal combination of arguments
      def build: FlatExpr => FlatExpr = Color(_name, _r, _g, _b, _alpha)
    }
  }
  case class Offset(radius: Option[NumericExpr],
                    delta: Option[NumericExpr],
                    chamfer: Option[BooleanExpr])(val fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] = {
      val sb = StringBuilder.newBuilder.append("offset(")
      if (radius.nonEmpty) sb.append(radius.get)
      if (delta.nonEmpty) sb.append(delta.get)
      if (chamfer.nonEmpty) sb.append(s",${chamfer.get}")
      sb.append("){")
      Seq(sb.toString()) ++
        ToScad.indent(fe.toScad) ++
        Seq("}")
    }
  }
  object Offset {
    case class Builder(radius: Option[NumericExpr]=None,
                       delta: Option[NumericExpr]=None,
                       chamfer: Option[BooleanExpr]=None) {
      def radius(radius: NumericExpr): Builder = this.copy(radius = Some(radius))
      def delta(delta: NumericExpr): Builder = this.copy(delta = Some(delta))
      def chamfer(chamfer: BooleanExpr): Builder = this.copy(chamfer = Some(chamfer))
      // TODO: thow exception when building with illegal combination of arguments
      def build: FlatExpr => FlatExpr = Offset(radius, delta, chamfer)
    }
  }
  case class Hull(fes: Seq[FlatExpr]) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq("hull(){") ++
        ToScad.indent(fes.flatMap(_.toScad)) ++
        Seq("}")
  }
  case class Union(fes: Seq[FlatExpr]) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq("union(){") ++
        ToScad.indent(fes.flatMap(_.toScad)) ++
        Seq("}")
  }
  case class Difference(startWith: FlatExpr, subtractAll: Seq[FlatExpr]) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq("difference(){") ++
        ToScad.indent(startWith.toScad) ++
        ToScad.indent(subtractAll.flatMap(_.toScad)) ++
        Seq("}")
  }
  case class Intersection(fes: Seq[FlatExpr]) extends FlatExpr {
    override def toScad: Seq[String] =
      Seq("intersection(){") ++
        ToScad.indent(fes.flatMap(_.toScad)) ++
        Seq("}")
  }

  class Modifier(modifier: Char, fe: FlatExpr) extends FlatExpr {
    override def toScad: Seq[String] = {
      val enabled = fe.toScad
      (modifier + enabled.head) +: enabled.tail
    }
  }
  case class Disable   (fe: FlatExpr) extends Modifier('*', fe)
  case class ShowOnly  (fe: FlatExpr) extends Modifier('!', fe)
  case class Highlight (fe: FlatExpr) extends Modifier('#', fe)
  case class Background(fe: FlatExpr) extends Modifier('%', fe)

}