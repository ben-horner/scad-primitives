package bkh.scad

import bkh.scad.bool.Bool.FALSE
import bkh.scad.numeric.Num._
import bkh.scad.flat.FlatExpr._
import bkh.scad.flat.VectorExpr.Dot
import bkh.scad.bool.BooleanExpr
import bkh.scad.numeric.{NumVar, NumericExpr}

package object flat {
  def vec(x: NumericExpr, y: NumericExpr): VectorExpr = Vec(x, y)
  def dot(ve1: VectorExpr, ve2: VectorExpr): NumericExpr = Dot(ve1, ve2).simplify

  def pnt(x: NumericExpr, y: NumericExpr): PointExpr = Pnt(Vec(x, y))

  def circle(radius: NumericExpr): CircleExpr = CircleExpr(radius)
  def square(size: NumericExpr): RectangleExpr = RectangleExpr(size, size)
  def rectangle(width: NumericExpr, height: NumericExpr): RectangleExpr = RectangleExpr(width, height)
  def polygon(points: Seq[PointExpr]): PolygonExpr = PolygonExpr(points)
  def polygon(points: Seq[PointExpr], paths: Seq[Seq[Int]]): PolygonExpr = PolygonExpr(points, Some(paths))
  def text(text: String): TextExpr = TextExpr(text)

  def arrow(tail: PointExpr, head: PointExpr): FlatExpr = {
    val v: VectorExpr = head - tail
    val unitClockPerp = v.clockPerp / v.clockPerp.length
    polygon(Seq(tail + unitClockPerp/2, tail - unitClockPerp/2, head))
  }

  def translate(vector: VectorExpr)(fe: FlatExpr): FlatExpr = fe.translated(vector)
  def rotate(angle: NumericExpr)(fe: FlatExpr): FlatExpr = fe.rotated(angle)
  def scale(x: NumericExpr, y: NumericExpr)(fe: FlatExpr): FlatExpr = fe.scaled(x, y)
  def mirror(vector: VectorExpr)(fe: FlatExpr): FlatExpr = fe.mirrored(vector)
  def resize(x: NumericExpr, y: NumericExpr)(fe: FlatExpr): FlatExpr = fe.resized(x, y)
  def unionFor(loopVar: NumVar, start: NumericExpr, step: NumericExpr, end: NumericExpr)(fe: FlatExpr): UnionFor =
    UnionFor(loopVar, start, step, end)(fe)
  def unionFor(loopVar: NumVar, start: NumericExpr, end: NumericExpr)(fe: FlatExpr): UnionFor =
    UnionFor(loopVar, start, 1, end)(fe)
  def color(name: String, alpha: NumericExpr=1)(fe: FlatExpr): FlatExpr =
    fe.colored(name, alpha)
  def color(r: NumericExpr, g: NumericExpr, b: NumericExpr)(fe: FlatExpr): FlatExpr =
    fe.colored(r, b, g)
  def color(r: NumericExpr, g: NumericExpr, b: NumericExpr, alpha: NumericExpr)(fe: FlatExpr): FlatExpr =
    fe.colored(r, b, g, alpha)
  def offset(radius: NumericExpr)(fe: FlatExpr): FlatExpr =
    fe.offsetted(radius)
  def offset(delta: NumericExpr, chamfer: BooleanExpr = FALSE)(fe: FlatExpr): FlatExpr =
    fe.offsetted(delta, chamfer)
  def linearExtrude(height: NumericExpr)(fe: FlatExpr): bkh.scad.solid.SolidExpr = fe.linearExtruded(height)
  def rotateExtrude(angle: NumericExpr)(fe: FlatExpr): bkh.scad.solid.SolidExpr = fe.rotateExtruded(angle)

  // unfortunately the functions below (which allow the use of lists of things) can't use curly braces {}
  def hull(fes: FlatExpr*): Hull = Hull(fes)
  def union(fes: FlatExpr*): Union = Union(fes)
  def difference(startWith: FlatExpr, subtractAll: FlatExpr*): Difference = Difference(startWith, subtractAll)
  def intersection(fes: FlatExpr*): Intersection = Intersection(fes)

  def disable   (fe: FlatExpr): Disable = Disable(fe)
  def showOnly  (fe: FlatExpr): ShowOnly = ShowOnly(fe)
  def highlight (fe: FlatExpr): Highlight = Highlight(fe)
  def background(fe: FlatExpr): Background = Background(fe)
}
