package bkh.scad.flat

case class VecVar(name: String, valueOpt: Option[VectorExpr] = None) extends VectorExpr {
  override def value: Vec = {
    require(valueOpt.isDefined, s"cannot get value for VecVar with name $name is abstract, a name only")
    valueOpt.get.value
  }
  override def simplify: VectorExpr = this
  override def toString: String = name
}
