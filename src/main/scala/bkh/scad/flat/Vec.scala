package bkh.scad.flat

import bkh.scad.solid
import bkh.scad.numeric.Num._
import bkh.scad.numeric.NumericExpr

case class Vec(override val x: NumericExpr, override val y: NumericExpr) extends VectorExpr {

  override def value: Vec = this

  override def unary_- : Vec = Vec(-x, -y)
  override def unary_+ : Vec = this
  override def * (scalar: NumericExpr): Vec = Vec(this.x * scalar, this.y * scalar)
  override def *:(scalar: NumericExpr): Vec = this * scalar
  override def / (scalar: NumericExpr): Vec = this * (1 / scalar)
  def +(that: Vec): Vec = Vec(this.x + that.x, this.y + that.y)
  def -(that: Vec): Vec = this + -that
  def dot(that: Vec): NumericExpr = this.x * that.x + this.y * that.y
  def *(that: Vec): NumericExpr = this.dot(that)

  override def simplify: VectorExpr = this
  override def toString: String = s"[$x,$y]"
}

object Vec {
  val ZERO: Vec = Vec(0, 0)
  val X: Vec = Vec(1, 0)
  val Y: Vec = Vec(0, 1)

  implicit def toSolidVec(vec: VectorExpr): solid.VectorExpr = solid.Vec(vec.x, vec.y, 0.0)

}
