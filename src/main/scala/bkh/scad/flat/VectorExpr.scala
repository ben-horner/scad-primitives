package bkh.scad.flat

import bkh.scad.ToScad
import bkh.scad.numeric._

trait VectorExpr extends ToScad {
  import VectorExpr._

  def value: Vec

  def unary_- : VectorExpr = Negation(this).simplify
  def unary_+ : VectorExpr = this
  def *  (scalar: NumericExpr): VectorExpr = Mult(this, scalar).simplify
  def *: (scalar: NumericExpr): VectorExpr = this * scalar
  def /  (scalar: NumericExpr): VectorExpr = Div(this, scalar).simplify
  def +  (that: VectorExpr): VectorExpr  = Plus(this, that).simplify  // TODO: replace Plus with Sum
  def -  (that: VectorExpr): VectorExpr  = Minus(this, that).simplify // TODO: replace Minus with Sum
  def dot(that: VectorExpr): NumericExpr = Dot(this, that).simplify
  def *  (that: VectorExpr): NumericExpr = dot(that)
  // note that cross product is actually only defined for 3d vectors.
  // this function returns the magnitude of the cross product of these 2d vectors in 3d (where their z=0)
  def cross(that: VectorExpr): NumericExpr = this.x * that.y - this.y * that.x
  def x(that: VectorExpr): NumericExpr = this.cross(that)

  def x: NumericExpr = GetX(this).simplify
  def y: NumericExpr = GetY(this).simplify

  def unit: VectorExpr = this/length
  def length: NumericExpr = (this * this).sqrt
  def withLength(length: NumericExpr): VectorExpr = this.unit * length

  def angleFrom(that: VectorExpr): NumericExpr = atan2(that.cross(this), that.dot(this))
  def scalarProj(that: VectorExpr): NumericExpr = this.dot(that.unit)
  def projParallel(that: VectorExpr): VectorExpr = that.unit * this.scalarProj(that)
  def projPerp(that: VectorExpr): VectorExpr = this - projParallel(that)

  def clockPerp: VectorExpr = Vec(this.y, -this.x)
  def counterClockPerp: VectorExpr = Vec(-this.y, this.x)

  def rotated(angle: NumericExpr): VectorExpr = {
    val angleFromX = atan2(y, x)
    val newAngleFromX = angleFromX + angle
    val radius = sqrt(x*x + y*y)
    Vec(radius * cos(newAngleFromX), radius * sin(newAngleFromX))
  }

  def simplify: VectorExpr
}

object VectorExpr {

  case class GetX(ve: VectorExpr) extends NumericExpr {
    override def value: Num = ve.value.x.value
    override def simplify: NumericExpr = ve match {
      case v: Vec => v.x
      case simplified => this
    }
    override def toString: String = s"($ve)[0]"
  }

  case class GetY(ve: VectorExpr) extends NumericExpr {
    override def value: Num = ve.value.y.value
    override def simplify: NumericExpr = ve match {
      case v: Vec => v.y
      case simplified => this
    }
    override def toString: String = s"($ve)[1]"
  }

  case class Negation(ve: VectorExpr) extends UnaryOpVectorExpr(ve, -_, "-")
  case class Mult(ve: VectorExpr, ne: NumericExpr) extends FuncVSVectorExpr(ve, ne, _ * _, "*")
  case class Div(ve: VectorExpr, ne: NumericExpr) extends FuncVSVectorExpr(ve, ne, _ / _, "/")
  case class Plus(ve1: VectorExpr, ve2: VectorExpr) extends BinaryOpVectorExpr(ve1, ve2, _ + _, "+")
  case class Minus(ve1: VectorExpr, ve2: VectorExpr) extends BinaryOpVectorExpr(ve1, ve2, _ - _, "-")
  case class Dot(ve1: VectorExpr, ve2: VectorExpr) extends BinaryOpNumericExpr(ve1, ve2, _ * _, "*")

  class UnaryOpVectorExpr(ve: VectorExpr, f: Vec => Vec, symbol: String) extends VectorExpr {
    override def value: Vec = f(ve.value)
    override def simplify: VectorExpr = ve match {
      case v: Vec => f(v)
      case simplified => simplified
    }
    override def toString: String = s"($symbol$ve)"
  }

  class BinaryOpVectorExpr(ve1: VectorExpr, ve2: VectorExpr, f: (Vec, Vec) => Vec, symbol: String) extends VectorExpr {
    override def value: Vec = f(ve1.value, ve2.value)
    override def simplify: VectorExpr = (ve1, ve2) match {
      case (v1: Vec, v2: Vec) => f(v1, v2)
      case simplified => this
    }
    override def toString: String = s"($ve1$symbol$ve2)"
  }

  //TODO: should f() be (Vec, Num) => Vec (instead of NumericExpr)
  class FuncVSVectorExpr(ve: VectorExpr, scalar: NumericExpr, f: (Vec, NumericExpr) => Vec, name: String) extends VectorExpr {
    override def value: Vec = f(ve.value, scalar)
    override def simplify: VectorExpr = ve match {
      case v: Vec => f(v, scalar)
      case simplified => simplified
    }
    override def toString: String = s"($ve$name$scalar)"
  }

  class BinaryOpNumericExpr(ve1: VectorExpr, ve2: VectorExpr, f: (Vec, Vec) => NumericExpr, symbol: String) extends NumericExpr {
    override def value: Num = f(ve1.value, ve2.value).value
    override def simplify: NumericExpr = (ve1, ve2) match {
      case (v1: Vec, v2: Vec) => f(v1, v2)
      case simplified => this
    }
    override def toString: String = s"($ve1$symbol$ve2)"
  }

}
