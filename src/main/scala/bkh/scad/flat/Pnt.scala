package bkh.scad.flat

import bkh.scad.flat.Vec.{ZERO, toSolidVec}
import bkh.scad.solid
import bkh.scad.numeric.Num._

case class Pnt(v: VectorExpr) extends PointExpr {

  override def value: Pnt = this

  override def +(ve: VectorExpr): Pnt = Pnt(this.v + ve)
  override def -(ve: VectorExpr): Pnt = Pnt(this.v - ve)
  def -(that: Pnt): VectorExpr = that.v - this.v

  override def simplify: PointExpr = this
  override def toString: String = v.toString
}

object Pnt {
  val ORIGIN: Pnt = Pnt(ZERO)

  implicit def toSolidPnt(pnt: PointExpr): solid.PointExpr = solid.Pnt(solid.Vec(pnt.x, pnt.y, 0.0))

}
