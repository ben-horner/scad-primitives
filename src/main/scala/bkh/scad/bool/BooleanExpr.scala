package bkh.scad.bool

import bkh.scad.ToScad

// TODO: include better simplification according to logical properties, isolate variables away from collapsable expressions
// (x || y) == (y || x)
// !!x == x
// x && !x = false
// ...
trait BooleanExpr extends ToScad {
  import BooleanExpr._

  def value: Bool

  def unary_! : BooleanExpr = Not(this).simplify
  def ||(that: BooleanExpr): BooleanExpr = Or (this, that).simplify
  def &&(that: BooleanExpr): BooleanExpr = And(this, that).simplify
  def simplify: BooleanExpr
}

object BooleanExpr {

  case class Not(be: BooleanExpr) extends UnaryOpBooleanExpr(be, ! _, "!")
  case class Or(be1: BooleanExpr, be2: BooleanExpr) extends InfixBinaryOpBooleanExpr(be1, be2, _ || _, "||")
  case class And(be1: BooleanExpr, be2: BooleanExpr) extends InfixBinaryOpBooleanExpr(be1, be2, _ && _, "&&")

  class UnaryOpBooleanExpr(be: BooleanExpr, f: Bool => Bool, symbol: String) extends BooleanExpr {
    override def value: Bool = f(be.value)
    override def simplify: BooleanExpr = be match {
      case b: Bool => f(b)
      case simplified => this
    }
    override def toString: String = s"($symbol${be.toString})"
  }

  class InfixBinaryOpBooleanExpr(be1: BooleanExpr, be2: BooleanExpr, f: (Bool, Bool) => Bool, symbol: String) extends BooleanExpr {
    override def value: Bool = f(be1.value, be2.value)
    override def simplify: BooleanExpr = (be1, be2) match {
      case (b1: Bool, b2: Bool) => f(b1, b2)
      case simplified => this
    }
    override def toString = s"(${be1.toString}$symbol${be2.toString})"
  }

}
