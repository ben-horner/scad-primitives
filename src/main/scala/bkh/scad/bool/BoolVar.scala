package bkh.scad.bool

case class BoolVar(name: String, valueOpt: Option[BooleanExpr] = None) extends BooleanExpr {
  override def value: Bool = valueOpt.get.value
  override def simplify: BooleanExpr = this
  override def toString: String = name
}
