package bkh.scad.bool

case class Bool(b: Boolean) extends BooleanExpr {

  override def value: Bool = this

  override def unary_! : Bool = Bool(!b)
  def ||(that: Bool): Bool = Bool(this.b || that.b)
  def &&(that: Bool): Bool = Bool(this.b && that.b)
  override def simplify: BooleanExpr = this
  override def toString = b.toString
}

object Bool {
  val TRUE: Bool = Bool(true)
  val FALSE: Bool = Bool(false)

  implicit def booleanToBool(b: Boolean): Bool = Bool(b)
}