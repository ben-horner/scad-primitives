package bkh.scad

package object numeric {

  // TODO: add the rest of the functions here, just passing off to instance methods
  def atan2(y: NumericExpr, x: NumericExpr): NumericExpr = NumericExpr.Atan2(y, x)

  // // TODO: complete this pattern for the rest...
  def sin(ne: NumericExpr): NumericExpr = ne.sin
  def cos(ne: NumericExpr): NumericExpr = ne.cos
  def abs(ne: NumericExpr): NumericExpr = ne.abs
  def sqrt(ne: NumericExpr): NumericExpr = ne.sqrt

}
