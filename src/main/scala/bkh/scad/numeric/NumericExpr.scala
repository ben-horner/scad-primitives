package bkh.scad.numeric

import bkh.scad.ToScad

// TODO: include better simplification according to mathematicla properties, isolate variables away from collapsable expressions
trait NumericExpr extends ToScad {
  import NumericExpr._

  def value: Num

  def unary_- : NumericExpr = Negate(this).simplify
  def unary_+ : NumericExpr = this

  def +(that: NumericExpr): NumericExpr = Plus (this, that).simplify // TODO: replace Plus with Sum
  def -(that: NumericExpr): NumericExpr = Minus(this, that).simplify // TODO: replace Minus with Sum (and Negate)
  def *(that: NumericExpr): NumericExpr = Mult (this, that).simplify // TODO: replace Mult with Product
  def /(that: NumericExpr): NumericExpr = Div  (this, that).simplify // TODO: replace Div with Product (and Reciprocal?)
  def %(that: NumericExpr): NumericExpr = Mod  (this, that).simplify

  def pow(that: NumericExpr): NumericExpr = Pow(this, that).simplify

  def abs  : NumericExpr = Abs  (this).simplify
  def sign : NumericExpr = Sign (this).simplify
  def sin  : NumericExpr = Sin  (this).simplify
  def cos  : NumericExpr = Cos  (this).simplify
  def tan  : NumericExpr = Tan  (this).simplify
  def acos : NumericExpr = Acos (this).simplify
  def asin : NumericExpr = Asin (this).simplify
  def atan : NumericExpr = Atan (this).simplify
  def floor: NumericExpr = Floor(this).simplify
  def round: NumericExpr = Round(this).simplify
  def ceil : NumericExpr = Ceil (this).simplify
  def ln   : NumericExpr = Ln   (this).simplify
  def log  : NumericExpr = Log  (this).simplify
  def sqrt : NumericExpr = Sqrt (this).simplify
  def exp  : NumericExpr = Exp  (this).simplify

  def simplify: NumericExpr
}

object NumericExpr {
  case class Negate(ne: NumericExpr) extends UnaryOpNumericExpr(ne, -_, "-")
  case class Plus (ne1: NumericExpr, ne2: NumericExpr) extends BinaryOpNumericExpr(ne1, ne2, _ + _, "+")
  case class Minus(ne1: NumericExpr, ne2: NumericExpr) extends BinaryOpNumericExpr(ne1, ne2, _ - _, "-")
  case class Mult (ne1: NumericExpr, ne2: NumericExpr) extends BinaryOpNumericExpr(ne1, ne2, _ * _, "*")
  case class Div  (ne1: NumericExpr, ne2: NumericExpr) extends BinaryOpNumericExpr(ne1, ne2, _ / _, "/")
  case class Mod  (ne1: NumericExpr, ne2: NumericExpr) extends BinaryOpNumericExpr(ne1, ne2, _ % _, "%")

  case class Pow (base: NumericExpr, exponent: NumericExpr) extends Func2NumericExpr(base, exponent, _.pow(_), "pow")
  case class Atan2(y: NumericExpr, x: NumericExpr) extends Func2NumericExpr(y, x, Num.atan2, "atan2")

  case class Min(ne1: NumericExpr, ne2: NumericExpr) extends Func2NumericExpr(ne1, ne2, Num.min, "min")
  case class Max(ne1: NumericExpr, ne2: NumericExpr) extends Func2NumericExpr(ne1, ne2, Num.max, "max")

  case class Abs  (ne: NumericExpr) extends Func1NumericExpr(ne, _.abs,   "abs"  )
  case class Sign (ne: NumericExpr) extends Func1NumericExpr(ne, _.sign,  "sign" )
  case class Sin  (ne: NumericExpr) extends Func1NumericExpr(ne, _.sin,   "sin"  )
  case class Cos  (ne: NumericExpr) extends Func1NumericExpr(ne, _.cos,   "cos"  )
  case class Tan  (ne: NumericExpr) extends Func1NumericExpr(ne, _.tan,   "tan"  )
  case class Acos (ne: NumericExpr) extends Func1NumericExpr(ne, _.acos,  "acos" )
  case class Asin (ne: NumericExpr) extends Func1NumericExpr(ne, _.asin,  "asin" )
  case class Atan (ne: NumericExpr) extends Func1NumericExpr(ne, _.atan,  "atan" )
  case class Floor(ne: NumericExpr) extends Func1NumericExpr(ne, _.floor, "floor")
  case class Round(ne: NumericExpr) extends Func1NumericExpr(ne, _.round, "round")
  case class Ceil (ne: NumericExpr) extends Func1NumericExpr(ne, _.ceil,  "ceil" )
  case class Ln   (ne: NumericExpr) extends Func1NumericExpr(ne, _.ln,    "ln"   )
  case class Log  (ne: NumericExpr) extends Func1NumericExpr(ne, _.log,   "log"  )
  case class Sqrt (ne: NumericExpr) extends Func1NumericExpr(ne, _.sqrt,  "sqrt" )
  case class Exp  (ne: NumericExpr) extends Func1NumericExpr(ne, _.exp,   "exp"  )

  class UnaryOpNumericExpr(ne: NumericExpr, f: Num => Num, symbol: String) extends NumericExpr {
    override def value: Num = f(ne.value)
    override def simplify: NumericExpr = ne match {
      case n: Num => f(n)
      case simplified => this
    }
    override def toString: String = {
      val result = s"($symbol$ne)"
      require(Option(symbol).nonEmpty, s"UnaryOpNumericExpr missing symbol: $result")
      require(Option(ne).nonEmpty, s"UnaryOpNumericExpr missing expression: $result")
      result
    }
  }

  class BinaryOpNumericExpr(ne1: NumericExpr, ne2: NumericExpr, f:(Num, Num) => Num, symbol: String) extends NumericExpr {
    override def value: Num = {
      f(ne1.value, ne2.value).d
    }
    override def simplify: NumericExpr = (ne1, ne2) match {
      case (n1: Num, n2: Num) => f(n1, n2)
      case simplified => this
    }
    override def toString: String = {
      val result = s"($ne1$symbol$ne2)"
      require(Option(ne1).nonEmpty, s"BinaryOpNumericExpr missing first expression: $result")
      require(Option(symbol).nonEmpty, s"BinaryOpNumericExpr missing symbol: $result")
      require(Option(ne2).nonEmpty, s"BinaryOpNumericExpr missing second expression: $result")
      result
    }
  }

  class Func1NumericExpr(ne1: NumericExpr, f: Num => Num, name: String) extends NumericExpr {
    override def value: Num = f(ne1.value)
    override def simplify: NumericExpr = ne1 match {
      case n: Num => f(n)
      case simplified => this
    }
    override def toString: String = {
      val result = s"$name($ne1)"
      require(Option(name).nonEmpty, s"Func1NumericExpr missing name: $result")
      require(Option(ne1).nonEmpty, s"Func1NumericExpr missing expression: $result")
      result
    }
  }

  class Func2NumericExpr(ne1: NumericExpr, ne2: NumericExpr, f: (Num, Num) => Num, name: String) extends NumericExpr {
    override def value: Num =  f(ne1.value, ne2.value)
    override def simplify: NumericExpr = (ne1, ne2) match {
      case (n1: Num, n2: Num) => f(n1, n2)
      case simplified => this
    }
    override def toString: String = {
      val result = s"$name($ne1,$ne2)"
      require(Option(name).nonEmpty, s"Func2NumericExpr missing name: $result")
      require(Option(ne1).nonEmpty, s"Func2NumericExpr missing first expression: $result")
      require(Option(ne2).nonEmpty, s"Func2NumericExpr missing second expression: $result")
      result
    }
  }

}
