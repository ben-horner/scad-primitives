package bkh.scad.numeric

import scala.math

case class Num(d: Double) extends NumericExpr {
  override def value: Num = this

  override def unary_- : Num = Num(-d)
  override def unary_+ : Num = this

  def +(that: Num): Num = Num(this.d + that.d)
  def -(that: Num): Num = Num(this.d - that.d)
  def *(that: Num): Num = Num(this.d * that.d)
  def /(that: Num): Num = Num(this.d / that.d)
  def %(that: Num): Num = Num(this.d % that.d)

  def pow(exp: Num): Num = Num(math.pow(d, exp.d))

  override def abs  : Num = Num(math.abs(d))
  override def sign : Num = Num(math.signum(d))
  override def sin  : Num = Num(math.sin(d * math.Pi / 180.0))  // OpenSCAD will make d degrees, convert to radians
  override def cos  : Num = Num(math.cos(d * math.Pi / 180.0))  // OpenSCAD will make d degrees, convert to radians
  override def tan  : Num = Num(math.tan(d * math.Pi / 180.0))  // OpenSCAD will make d degrees, convert to radians
  override def acos : Num = Num(math.acos(d) * 180.0 / math.Pi) // OpenSCAD will want degrees back, convert to degrees
  override def asin : Num = Num(math.asin(d) * 180.0 / math.Pi) // OpenSCAD will want degrees back, convert to degrees
  override def atan : Num = Num(math.atan(d) * 180.0 / math.Pi) // OpenSCAD will want degrees back, convert to degrees
  override def floor: Num = Num(math.floor(d))
  override def round: Num = Num(math.round(d).toDouble)
  override def ceil : Num = Num(math.ceil(d))
  override def ln   : Num = Num(math.log(d))
  override def log  : Num = Num(math.log10(d))
  override def sqrt : Num = Num(math.sqrt(d))
  override def exp  : Num = Num(math.exp(d))

  override def simplify: NumericExpr = this
  override def toString: String = d.toString
}

object Num {
  def atan2(y: Num, x: Num): Num = Num(math.atan2(y.d, x.d) * 180.0 / math.Pi) // OpenSCAD will want degrees back, convert to degrees

  def min(n1: Num, n2: Num): Num = Num(math.min(n1.d, n2.d))
  def max(n1: Num, n2: Num): Num = Num(math.max(n1.d, n2.d))

  val ZERO: Num = Num(0)
  val PI: Num = Num(3.14159265359)

  implicit def doubleToNum(d: Double): Num = Num(d)
  implicit def floatToNum(f: Float) : Num = Num(f)
  implicit def intToNum(i: Int)   : Num = Num(i)
  implicit def longToNum(l: Long)  : Num = Num(l.toDouble)
}