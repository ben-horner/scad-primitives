package bkh.scad.numeric

case class NumVar(name: String, valueOpt: Option[NumericExpr] = None) extends NumericExpr {
  assert(name.trim != "", "A variable with a blank string for a name can't be referenced in generated OpenSCAD code.")
  override def value: Num = valueOpt.get.value
  override def simplify: NumVar = this
  override def toString: String = {
    require(Option(name).nonEmpty, s"variable missing name: $this")
    name
  }
}

object NumVar {
  implicit def stringToVarNum(name: String) = NumVar(name)

  def apply(name: String) = new NumVar(name)
  def apply(name: String, value: NumericExpr) = new NumVar(name, Some(value))
}