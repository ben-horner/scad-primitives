package bkh.scad

import java.io.{File, FileWriter, PrintWriter}

trait ToScad {
  def toScad: Seq[String] = Seq(this.toString)
  def saveScad(toFile: File): Unit = {
    val pw = new PrintWriter(new FileWriter(toFile))
    toScad.foreach(pw.println)
    pw.close()
  }
}


object ToScad {
  def indent(lines: Seq[String], prefix: String = "  "): Seq[String] =
    lines.map(line => s"$prefix$line")

  def mkStringSeq(seq: Seq[String], start: String, sep: String, end: String): Seq[String] =
    start +: mkStringSeq(seq, sep) :+ end

  def mkStringSeq(seq: Seq[String], sep: String): Seq[String] =
    if (seq.size <= 1) {
      seq
    } else {
      (seq.take(1) :+ sep) ++ mkStringSeq(seq.drop(1), sep)
    }
}