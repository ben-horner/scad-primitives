package bkh.scad

import java.lang.reflect.{Constructor, Field}

import bkh.scad.bool.{BoolVar, BooleanExpr}
import bkh.scad.numeric.{NumVar, NumericExpr}

import scala.reflect.{ClassTag, _}

object Modules {
  var definedModules: Set[Class[_]] = Set()

  // turn a case class into a module definition
  // turn a case class instance into a module call

  // want instance to be a case class (Product) and ToScad
  def toModuleDef[TS <: ToScad: ClassTag]: Seq[String] = {
    val clazz: Class[_] = classTag[TS].runtimeClass

    val constructors: Array[Constructor[_]] = clazz.getConstructors()
    require(constructors.size == 1, s"toModuleDef currently only supports case classes with a single constructor, found ${constructors.size} constructors: ${constructors.mkString("\n\t", "\n\t", "")}")
    val constructor = constructors(0)

    val classFields = clazz.getDeclaredFields
    val constructorParams = constructor.getParameterTypes
    require(classFields.size == constructorParams.size, s"toModuleDef currently only supports case classes (${clazz.getCanonicalName}) which have the same number of fields (${classFields.size}) and constructor parameters (${constructorParams.size})")
    require(classFields.map(_.getType).sameElements(constructorParams),
      s"""|toModuleDef currently only supports case classes whose constructor arguments match the class fields
          |    class fields: ${classFields.mkString(", ")}
          |    constructor params: ${constructorParams.mkString(", ")}""".stripMargin)

    def getVarForField(field: Field): AnyRef = {
      var fieldTypeName: String = field.getType.getCanonicalName
      if (BooleanExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        BoolVar(field.getName)
      } else if (NumericExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        NumVar(field.getName)
      } else

      if (flat.VectorExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        flat.VecVar(field.getName)
      } else if (flat.PointExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        flat.PntVar(field.getName)
      } else if (flat.FlatExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        flat.VarFlat(field.getName)
      } else

      if (solid.VectorExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        solid.VecVar(field.getName)
      } else if (solid.PointExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        solid.PntVar(field.getName)
      } else if (solid.SolidExpr.getClass.getCanonicalName.startsWith(fieldTypeName)) {
        solid.VarSolid(field.getName)
      }

      else {
        throw new IllegalStateException(s"unknown case class member type for module: $field")
      }
    }

    val nameToVar = classFields.map{ field =>
      field.getName -> getVarForField(field)
    }

    var result = Seq[String]()
    result = result :+ s"module ${clazz.getSimpleName}(${nameToVar.unzip._1.mkString(", ")}) {"
    val newInstance = constructor.newInstance(nameToVar.unzip._2:_*).asInstanceOf[ToScad]
    result = result ++ ToScad.indent(newInstance.toScad)
    result = result :+ "}"
    result
  }

  def toModuleCall[TS <: ToScad: ClassTag](instance: TS): Seq[String] = {
    val clazz: Class[_] = classTag[TS].runtimeClass
    var result: Seq[String] = if (!definedModules.contains(clazz)) {
      definedModules = definedModules + clazz
      toModuleDef[TS]
    } else {
      Seq()
    }

    val constructors: Array[Constructor[_]] = clazz.getConstructors()
    require(constructors.size == 1, s"toModuleDef currently only supports case classes with a single constructor, found ${constructors.size} constructors: ${constructors.mkString("\n\t", "\n\t", "")}")
    val constructor = constructors(0)

    val classFields = clazz.getDeclaredFields
    val constructorParams = constructor.getParameterTypes
    require(classFields.size == constructorParams.size, s"toModuleDef currently only supports case classes (${clazz.getCanonicalName}) which have the same number of fields (${classFields.size}) and constructor parameters (${constructorParams.size})")
    require(classFields.map(_.getType).sameElements(constructorParams),
      s"""|toModuleDef currently only supports case classes whose constructor arguments match the class fields
          |    class fields: ${classFields.mkString(", ")}
          |    constructor params: ${constructorParams.mkString(", ")}""".stripMargin)

    val paramValues = classFields.map{ field =>
      field.setAccessible(true)
      val scad = field.get(instance).asInstanceOf[ToScad].toScad
      (field.getName + " = " + scad(0)) +: scad.drop(1)
    }

    val paramToValueSep = paramValues.dropRight(1).map{ paramValue =>
      paramValue.dropRight(1) :+ (paramValue.last + ",")
    } ++ paramValues.takeRight(1)

    result = result :+ s"${clazz.getSimpleName} ("
    result = result ++ ToScad.indent(paramToValueSep.fold(Seq())(_ ++ _))
    result = result :+ ")"
    result
  }

}
