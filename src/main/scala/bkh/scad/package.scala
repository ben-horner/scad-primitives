package bkh

import bkh.scad.numeric.NumericExpr.{Atan2, Max, Min}
import bkh.scad.numeric.{Num, NumericExpr}

package object scad {
  val PI: Num = Num.PI

  def pow  (ne1: NumericExpr, ne2: NumericExpr): NumericExpr = ne1.pow(ne2)
  def atan2(ne1: NumericExpr, ne2: NumericExpr): NumericExpr = Atan2(ne1, ne2).simplify

  def min(ne1: NumericExpr, ne2: NumericExpr): NumericExpr = Min(ne1, ne2).simplify
  def max(ne1: NumericExpr, ne2: NumericExpr): NumericExpr = Max(ne1, ne2).simplify

  def abs  (ne: NumericExpr): NumericExpr = ne.abs
  def sign (ne: NumericExpr): NumericExpr = ne.sign
  def sin  (ne: NumericExpr): NumericExpr = ne.sin
  def cos  (ne: NumericExpr): NumericExpr = ne.cos
  def tan  (ne: NumericExpr): NumericExpr = ne.tan
  def acos (ne: NumericExpr): NumericExpr = ne.acos
  def asin (ne: NumericExpr): NumericExpr = ne.asin
  def atan (ne: NumericExpr): NumericExpr = ne.atan
  def floor(ne: NumericExpr): NumericExpr = ne.floor
  def round(ne: NumericExpr): NumericExpr = ne.round
  def ceil (ne: NumericExpr): NumericExpr = ne.ceil
  def ln   (ne: NumericExpr): NumericExpr = ne.ln
  def log  (ne: NumericExpr): NumericExpr = ne.log
  def sqrt (ne: NumericExpr): NumericExpr = ne.sqrt
  def exp  (ne: NumericExpr): NumericExpr = ne.exp

  // these guys produce numbers, but don't operate on numbers
  //    len()
  //    min()
  //    max()
  // rands generates a list of numbers
  //    rands()
  // let is only necessary in OpenSCAD function declarations
  //    let()
}
