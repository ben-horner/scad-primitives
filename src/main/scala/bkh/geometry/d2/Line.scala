package bkh.geometry.d2

import bkh.matrix.Matrix

import scala.math.abs

case class Line private (intercept: Pnt, dir: Vec) extends Transformable[Line] {
  require(abs(1.0 - dir.length) < 0.0000000001)
  require(intercept.dist(nearestTo(Pnt.ORIGIN)) < 0.0000000001)
  // TODO: disambiguate the two opposite directions with a require...

  override def transform(transformation: Transform): Line = {
    val p0 = intercept
    val p1 = intercept + dir
    val p0t = transformation(p0)
    val p1t = transformation(p1)
    Line(p0t, p1t - p0t)
  }

  def dist(p: Pnt): Double = (intercept - p).projPerp(dir).length

  def dist(that: Line): Double = {
    if (this.dir == that.dir)
      (this.intercept - that.intercept).projPerp(dir).length
    else
      0.0
  }

  def nearestTo(p: Pnt) = p + (intercept - p).projPerp(dir)

  // distance from other line (0 means collinear or intersecting)
  // closest point from this line to any point on that line (might be intersection)
  // angle between lines (0 <= angle <= 90)
  def intersect(that: Line): Option[Pnt] = {
    // intercept points p and q, direction vectors u and v, parameters s and t
    // line 1: s*u + p
    // line 2: t*v + q
    // set them equal:                          s*u + p = t*v + q
    // isolate variables (parameters s and t):  s*u - t*v = q - p
    // matrix form:
    // s * | u1 | - t * | v1 | = | q1 | - | p1 |
    //     | u2 |       | v2 |   | q2 |   | p2 |
    // rewrite with s and t as vector
    // | u1 -v1 | * | s | = | q1-p1 |
    // | u2 -v2 |   | t |   | q2-p2 |

    // write as augmented matrix
    // | u1 -v1 | q1-p1 |
    // | u2 -v2 | q2-p2 |

    val augmented = Matrix.fromCols(IndexedSeq(
      this.dir.toArray,
      (-that.dir).toArray,
      (that.intercept - this.intercept).toArray)
    )
    // reduced echelon hopeful gives us an identity matrix augmented with the solution for <s, t>
    val reducedEchelon = augmented.reducedEchelonForm
    assume(reducedEchelon.numRows == 2 && reducedEchelon.numCols == 3)
    if ((reducedEchelon.subMatrix(0 until 2, 0 until 2) - Matrix.identity(2)).sum > 0.0000000001) {
      // intersection is either the whole line, or empty.  either way not a point
      None
    } else {
      // plugging s and t back into the respective lines gives the same point of intersection
      assume((this.intercept + this.dir * reducedEchelon(0, 2)).dist(that.intercept + that.dir * reducedEchelon(1, 2)) < 0.0000000001,
        s"assumed that points would be equal after line intersection, but were ${(this.intercept + this.dir * reducedEchelon(0, 2)).dist(that.intercept + that.dir * reducedEchelon(1, 2))} apart")
      Some(this.intercept + this.dir * reducedEchelon(0, 2))
    }
  }

  override def toString: String = s"Line($intercept, $dir)"
}

object Line {

  val X_AXIS = Line(Pnt.ORIGIN, Vec.X)
  val Y_AXIS = Line(Pnt.ORIGIN, Vec.Y)

  def apply(intercept: Pnt, dir: Vec): Line = {
    val interceptNearestOrigin = Pnt(intercept.v.projPerp(dir))
    // TODO: pick between dir and -dir.  (whichever is closer to <1, 1>?)
    new Line(interceptNearestOrigin, dir.unit)
  }

  def apply(p1: Pnt, p2: Pnt): Line = {
    val dir = p2 - p1
    apply(p1, dir)
  }

}
