package bkh.geometry.d2

import bkh.geometry.d2
import bkh.geometry.d2.Pnt.toFlatPnt
import bkh.scad.flat
import bkh.scad.flat.{FlatExpr, polygon}

object Paths {

  def arc(p1: d2.Pnt, p2: d2.Pnt, p3: d2.Pnt, numPoints: Int, includeLastPoint: Boolean): Seq[d2.Pnt] = {
    require(p1.dist(p2) > 0)
    require(p1.dist(p3) > 0)
    require(p2.dist(p3) > 0)

    val s12 = Segment(p1, p2)
    val s23 = Segment(p2, p3)

    val s12b = s12.perpBisector
    val s23b = s23.perpBisector

    val center = s12b.intersect(s23b).get
    val radius = p1.dist(center)

    val v1 = p1 - center
    val v2 = p2 - center
    val v3 = p3 - center

    val a12 = {
      val a = v2.angleFrom(v1)
      if (a >= 0) a
      else 360 + a
    }
    val a13 = {
      val a = v3.angleFrom(v1)
      if (a >= 0) a
      else 360 + a
    }
    val angleDelta =
      if (a12 < a13) { // then we should wind with positve angle (counter-clockwise)
        a13 / numPoints
      } else { // we should wind with negative angle (clockwise)
        -(360-a13) / numPoints
      }

    val allButLast = for (i <- (0 until numPoints)) yield {
      v1.rotate(i * angleDelta).translate(center.v).toPnt
    }
    if (includeLastPoint) {
      allButLast :+ p3
    } else {
      allButLast
    }
  }

  def trace(path: Seq[d2.Pnt], tailThickness: Double, headThickness: Double): FlatExpr = {
    //in a sliding window of 2 through the path:
    //  form a trapezoid with base of tailThickness, and narrow end of headThickness
    // union them all together into a FlatExpr for rendering
    val segments = path.sliding(2).map{ pnts =>
      require(pnts.size == 2)
      val tail = pnts(0)
      val head = pnts(1)
      val d = head - tail
      val perp = {
        val alongX = d2.Vec.X.projPerp(d)
        val alongY = d2.Vec.Y.projPerp(d)
        if (alongX.length >= alongY.length) {
          alongX
        } else {
          alongY
        }
      }
      polygon(Seq(
        tail + perp.withLength(tailThickness/2),
        head + perp.withLength(headThickness/2),
        head - perp.withLength(headThickness/2),
        tail - perp.withLength(tailThickness/2)
      ).map(toFlatPnt))
    }.toSeq
    flat.union(segments:_*)
  }

}
