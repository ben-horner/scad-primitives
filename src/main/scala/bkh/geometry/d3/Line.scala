package bkh.geometry.d3

import bkh.matrix.Matrix

import scala.math.abs

case class Line private (intercept: Pnt, dir: Vec) extends Transformable[Line] {
  require(abs(1.0 - dir.length) < 0.0000000001)
  require(intercept.dist(nearestTo(Pnt.ORIGIN)) < 0.0000000001)
  // TODO: disambiguate the two opposite directions with a require...

  override def transform(transformation: Transform): Line = {
    val p0 = intercept
    val p1 = intercept + dir
    val p0t = transformation(p0)
    val p1t = transformation(p1)
    Line(p0t, p1t - p0t)
  }

  def dist(p: Pnt): Double = (intercept - p).projPerp(dir).length

  def dist(that: Line): Double = (this.intercept - that.intercept).projParallel(this.dir.cross(that.dir)).length

  def nearestTo(p: Pnt) = p + (intercept - p).projPerp(dir)

  // line 1: s*u + p  (this)
  // line 2: t*v + q  (that)
  // parameterized vector between any point on line 1 and any point on line 2
  // s*u - t*v + p - q
  // this vector must be simultaneously perpendicular to line 1, and perpendicular to line 2
  // that is, the dot product of the vector with the first line must be zero, and the second line also

  // s and t are the only variables, the rest are constants given two actual lines...
  // lets use notation a.b for the dot product of a and b
  // (s*u - t*v + p - q).u = 0
  // (s*u - t*v + p - q).v = 0

  // distribute dot product
  // s*u.u - t*v.u = -(p-q).u
  // s*u.v - t*v.v = -(p-q).v

  // | u.u -v.u | * | s | = | -(p-q).u |
  // | u.v -v.v |   | t |   | -(p-q).v |

  // | u1 u2 u3 |   | u1 v1 |   | 1  0 |   | s |   | u1 u2 u3 |   | q1-p1 |
  // | v1 v2 v3 | * | u2 v2 | * | 0 -1 | * | t | = | v1 v2 v3 | * | q2-p2 |
  //                | u3 v3 |                                     | q3-p3 |
  def nearestTo(that: Line): Option[Pnt] = {
    val negate2ndCol = Matrix.fromRows(IndexedSeq(
      IndexedSeq(1,  0),
      IndexedSeq(0, -1)
    ))
    val dirs = Matrix.fromRows(IndexedSeq(this.dir.toArray, that.dir.toArray))
    val intercepts = Matrix.fromCol((that.intercept - this.intercept).toArray)

    val system = (dirs * dirs.transpose * negate2ndCol).augmentCols(dirs * intercepts)
    val reducedEchelon = system.reducedEchelonForm
    assume(reducedEchelon.numRows == 2 && reducedEchelon.numCols == 3)
    if ((reducedEchelon.subMatrix(0 until 2, 0 until 2) - Matrix.identity(2)).sum > 0.0000000001) {
      None
    } else {
      Some(this.intercept + this.dir * reducedEchelon(0, 2))
    }
  }

  override def toString: String = s"Line($intercept, $dir)"
}

object Line {

  val X_AXIS = Line(Pnt.ORIGIN, Vec.X)
  val Y_AXIS = Line(Pnt.ORIGIN, Vec.Y)
  val Z_AXIS = Line(Pnt.ORIGIN, Vec.Z)

  def apply(intercept: Pnt, dir: Vec): Line = {
    val interceptNearestOrigin = Pnt(intercept.v.projPerp(dir))
    // TODO: pick between dir and -dir.  (whichever is closer to <1, 1, 1>?)
    new Line(interceptNearestOrigin, dir.unit)
  }

  def apply(p1: Pnt, p2: Pnt): Line = {
    val dir = p2 - p1
    apply(p1, dir)
  }

}
