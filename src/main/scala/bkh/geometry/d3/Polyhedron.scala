package bkh.geometry.d3

import bkh.scad.numeric.Num._
import bkh.scad.solid

case class Polyhedron(faces: Seq[Seq[Pnt]]) extends Transformable[Polyhedron] {

  override def transform(transformation: Transform): Polyhedron =
    Polyhedron(faces.map(_.map(transformation(_))))

  def insideOut: Polyhedron = {
    Polyhedron(faces.map(_.reverse))
  }

  // TODO: find the frontier of edges on the "outside" of the surface
  // can we come up with a multi-matching to close the polyhedron? (don't introduce new points if we can help it...)
//  def patch: Polyhedron = ???

}

object Polyhedron {

  def toSolidPolyhedron(polyhedron: Polyhedron): solid.PolyhedronExpr = {
    val points = polyhedron.faces.flatten.toSet.toSeq
    val pointToIndex = points.zipWithIndex.toMap
    solid.polyhedron(points.map(Pnt.toSolidPnt), polyhedron.faces.map(_.map(pointToIndex)), 10)
  }

}