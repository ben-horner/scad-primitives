package bkh.geometry.d3

import java.io.{FileWriter, PrintWriter}

import bkh.geometry.d3
import bkh.geometry.d3.Vec.toSolidVec
import bkh.scad.solid
import bkh.scad.solid._
import bkh.scad.solid.SolidExpr
import bkh.scad.numeric.Num._
import bkh.scad

object Paths {

  def trace(path: Seq[d3.Pnt], tailThickness: Double, headThickness: Double): SolidExpr = {
    //in a sliding window of 2 through the path:
    //  form a trapezoid with base of tailThickness, and narrow end of headThickness
    // union them all together into a FlatExpr for rendering
    val segments: Seq[SolidExpr] = path.sliding(2).map{ pnts =>
      require(pnts.size == 2)
      val tail = pnts(0).v
      val head = pnts(1).v
      val dir = head - tail
      val height = dir.length
      // uses Z for "forward" (the direction of the length of a cylinder)
      // first point cylinder at (lat, long) = (0.0, 0.0), then
      val (r, latitude, longitude) = dir.toSpherical
      cylinder(height, tailThickness, headThickness).
        rotated(0.0, 90.0, 0.0).
        rotated(0.0, -latitude, longitude).
        translated(tail)
    }.toSeq
    union(segments:_*)
  }

  def main(args: Array[String]): Unit = {
    val pw = new PrintWriter(new FileWriter("/home/ben/git/scad/scad-primitives/src/main/resources/shortPath.scad"))
    pw.println("//Hello World!")

    val path = Seq(d3.Pnt.ORIGIN, d3.Pnt(0.0, 0.0, 5.0), d3.Pnt(0.0, 0.0, 10.0))
    pw.println(Paths.trace(path, 0.25, 0.01).toScad.mkString("\n"))

    pw.println("//Goodbye World!")
    pw.close()
  }

}
