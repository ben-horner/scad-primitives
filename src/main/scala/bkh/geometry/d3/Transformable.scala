package bkh.geometry.d3

trait Transformable[A <: Transformable[A]] {

  def transform(transformation: Transform): A

  // interesting transformations providing a parameter for the center (all but translate)
  def translate(vec: Vec): A = transform(Transform.translate(vec))

  def scale(scaleXYZ: Double, center: Pnt): A = transform(Transform.scale(scaleXYZ, center))
  def scale(scaleX: Double, scaleY: Double, scaleZ: Double, center: Pnt): A = transform(Transform.scale(scaleX, scaleY, scaleZ, center))

  def pointReflect(center: Pnt): A = transform(Transform.pointReflect(center))

  def linearReflect(line: Line): A = transform(Transform.linearReflect(line))

  def planarReflect(plane: Plane): A = transform(Transform.planarReflect(plane))

  def rotate(degrees: Double, axis: Line): A = transform(Transform.rotate(degrees, axis))
  def rotateRadians(radians: Double, axis: Line): A = transform(Transform.rotateRadians(radians, axis))

  // transformations centered at origin
  def scale(scaleXYZ: Double): A = scale(scaleXYZ, scaleXYZ, scaleXYZ)
  def scale(scaleX: Double, scaleY: Double, scaleZ: Double): A = transform(Transform.scale(scaleX, scaleY, scaleZ))
  def originReflect: A = transform(Transform.originReflect)
  def linearReflect(dir: Vec): A = transform(Transform.linearReflect(dir))
  def planarReflect(perp: Vec): A = transform(Transform.planarReflect(perp))
  def rotate(fromDir: Vec, toDir: Vec): A = transform(Transform.rotate(fromDir,toDir))
  def rotate(fromDir: Vec, toDir: Vec, degrees: Double): A = transform(Transform.rotate(fromDir, toDir, degrees))
  def rotate(degrees: Double, axis: Vec): A = transform(Transform.rotate(degrees, axis))
  def rotateRadians(radians: Double, axis: Vec): A = transform(Transform.rotateRadians(radians, axis))

}
