package bkh.matrix

import org.scalatest.funspec.AnyFunSpec

class MatrixSpec extends AnyFunSpec {

  describe("A Matrix instance") {

    it("should ") {
      val zero5 = Matrix.zero(5, 5)
      assert(zero5 == zero5 * 10)
      assert(zero5 == 10 *: zero5)

      val random5 = Matrix.random(5, 5)
      assert(random5 != -random5)
      assert(random5 == -(-random5))
      assert(random5 != random5.transpose)
      assert(random5 == random5.transpose.transpose)

      assert(random5 == random5 + zero5)

      val identity5 = Matrix.identity(5)
      assert(identity5 == identity5.transpose)

      assert(random5 == random5 * identity5)
      assert(random5 == identity5 * random5)
    }

  }

}
