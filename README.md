# scad-primitives
Some additional functionality for dealing with lists, maps, vectors and transformations in OpenSCAD

I wrote what is here so far in the interest of trying to do some runtime type checking.  With the nestable map, it seems possible.  I've actually been through several iterations (I may check that into a branch).  It's not very satisfying though, and you can only get errors that describe the location of the error in modules, not in functions.

I've ended up thinking that just wrapping it in another language is a better option.  Then I can get all the power of that language.  I'm going to use scala.
