// can't echo text from functions.
// could abort from a function with infinite recursion, but there would be no explanation or line number.
// error checking can only be done from modules.  :(

module error(message, print=true) {
    if (print) {
        echo(str("ERROR: ", message));
        echo(str("***ABORTING*** (using infinite recursion to cause abort)"));
    }
    error("", false);
}

module require(condition, message) {
    if (!condition) error(message);
}


// TESTS (must manually verify)
/*
module testError(error) {
    if (error) {
        echo("testing error():  this branch should abort");
        error("testing error");
    } else {
        echo("testing error():  this branch should not abort");
    }
}

module testRequire(success) {
    if (success) {
        echo("testing require():  this branch should not abort");
        require(success, "condition succeeded, should not see this message, should not abort");
    } else {
        echo("testing require():  this branch should abort");
        require(success, "condition failed, should see this message, should abort");
    }
}

testError(false);
//testError(true);
testRequire(true);
//testRequire(false);
//*/